from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QHBoxLayout, QVBoxLayout, QRadioButton, QPushButton, QLabel, QWidget, QSizePolicy, QComboBox
from functools import partial

class ChooseValueDialog(QDialog):
    def __init__(self, parent, title, description, values):
        super(ChooseValueDialog, self).__init__(parent)
        self.setWindowTitle(title)
        
        self.selectedIndex = 0
        
        mainLayout = QVBoxLayout(self)
        mainLayout.setContentsMargins(15, 15, 10, 10)
        
        descLabel = QLabel(description, self)
        descLabel.setWordWrap(True)
        mainLayout.addWidget(descLabel)
        
        if len(values) <= 5:
            for i, aValue in enumerate(values):
                r = QRadioButton(unicode(aValue), self)
                if i == 0:
                    r.setChecked(True)
                r.clicked.connect(partial(self.select, i))
                mainLayout.addWidget(r)
        else:
            b = QComboBox(self)
            b.currentIndexChanged.connect(self.select)
            for aValue in values:
                b.addItem(unicode(aValue))
            mainLayout.addWidget(b)
            
        bottomWidget = QWidget(self)
        bottomLayout = QHBoxLayout(bottomWidget)
        bottomLayout.setContentsMargins(0, 0, 0, 0)
        
        cancelButton = QPushButton("Cancel")
        cancelButton.clicked.connect(self.reject)
        cancelButton.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Preferred)
        bottomLayout.addWidget(cancelButton, 1, Qt.AlignRight)
        
        okButton = QPushButton("OK")
        okButton.clicked.connect(self.accept)
        bottomLayout.addWidget(okButton, 0)
        
        mainLayout.addWidget(bottomWidget, 0, Qt.AlignBottom)
        
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
            
    def select(self, index):
        self.selectedIndex = index
    
if __name__ == '__main__':
        from PyQt5.QtWidgets import QApplication
        import sys
        app = QApplication(sys.argv)
        window = ChooseValueDialog(None, "Test", "Choose from the following values:", ["a", 2, False, 3.4, u"asdfasdf", ["asdf", "foo", 2]])
        window.show()
        window.activateWindow()
        window.raise_()
    
        sys.exit(app.exec_())