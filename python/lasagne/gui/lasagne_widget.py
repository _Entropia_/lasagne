# coding=utf-8

from PyQt5.QtWidgets import QDockWidget, QWidget, QVBoxLayout, QLabel, QMenu
from PyQt5.QtCore import QVariant, Qt, pyqtSlot, QThread, pyqtSignal, QPoint
from PyQt5.QtGui import QCursor
from lasagne import convert_string, log_exception, BASE_CATEGORIES
from functools import partial
from lasagne.network.network_handler import NetworkHandler
from lasagne.gui.choose_value_dialog import ChooseValueDialog
from lasagne.utils.topology import Topology
from lasagne.gui.query_bar import QueryBar
from lasagne.gui.results_table import ResultsTable, ResultsTableModel
import contextlib
import webbrowser

class LasagneWidget(QDockWidget):
    _getCategoryPaths = pyqtSignal(int, int)
    _getSimilarArticles = pyqtSignal(int)
    
    openNewView = pyqtSignal(int, int)  # page id, action index
    
    def __init__(self, client, parent):
        super(LasagneWidget, self).__init__("Lasagne", parent)

        self.networkThread = QThread(self)
        self.networkHandler = NetworkHandler(client)
        self.networkHandler.moveToThread(self.networkThread)
        self.networkThread.start()
            
        self._getCategoryPaths.connect(self.networkHandler.getCategoryPaths)
        self._getSimilarArticles.connect(self.networkHandler.getSimilarArticles)
        
        client.networkStatusChanged.connect(self.showConnectionInfo)
        
        self.networkHandler.statusChanged.connect(self.showStatus)
        self.networkHandler.queryResultsReceived.connect(self.showQueryResults)
        self.networkHandler.tableOutputReceived.connect(self.showTableOutput)
        self.networkHandler.choosePage.connect(self.choosePage)
        self.networkHandler.performActionOnID.connect(self.performActionOnID)
        self.networkHandler.queryFinished.connect(self.queryFinished)
            
        self.initUI()
        
    def closeEvent(self, event):
        self.networkHandler.close()
        self.networkThread.quit()
        return QDockWidget.closeEvent(self, event)
        
    def rankCell(self, _key, data, item):
        item.setData(QVariant(data[0]), Qt.DisplayRole)
        
    def rowCell(self, index, _key, data, item):
        if type(data[index]) == tuple:
            pID = data[index][0]
            text = unicode(data[index][1])
        else:
            pID = None
            text = unicode(data[index])
            
        item.setData(text, Qt.DisplayRole)
        if pID != None:
            item.setData(pID, ResultsTableModel.PAGE_ID_ROLE)
        
    def titleCell(self, _key, data, item):
        item.setData(unicode(data[1]), Qt.DisplayRole)
        
    def idCell(self, key, _data, item):
        item.setData(QVariant(key), Qt.DisplayRole)
        
    def typeCell(self, _key, data, item):
        item.setData("article" if data[2] == 0 else "cat" if data[2] == 14 else "unknown", Qt.DisplayRole)
    
    def categoriesCell(self, idx, _key, data, item):
        if idx >= len(data) or not data[idx]:
            item.setData("<no data>", Qt.DisplayRole)
        else:
            item.setData(", ".join(BASE_CATEGORIES[i] for i, val in enumerate(data[idx]) if val == 1), Qt.DisplayRole)
        
    @pyqtSlot(unicode)
    def showStatus(self, status):
        self.statusLabel.setText(status)
        
    @pyqtSlot(bool, unicode)
    def showConnectionInfo(self, connected, errorMessage):
        errorMessage = convert_string(errorMessage)
        if connected:
            self.showReady()
        else:
            self.showStatus(u"Error: " + errorMessage)
            
    def showReady(self):
        self.showStatus(u'Ready')
            
    def _formatPage(self, pID, pNs, pTitle):
        return "%s (%s, %d)" % (pTitle, "article" if pNs == 0 else "category", pID)
    
    @pyqtSlot(list, list, list)
    def showTableOutput(self, tableOutput, rowIDs, colIDs):
        try:
            tableModel = None
            for i, row in enumerate(tableOutput):
                if i == 0:
                    columns = []
                    for rowIdx, h in enumerate(row):
                        columns.append((h, partial(self.rowCell, rowIdx)))
                    tableModel = ResultsTableModel(self, columns)
                else:
                    tableModel.appendContentRow(i, row)
            if tableModel != None:
                if rowIDs:
                    tableModel.rowIDs = rowIDs
                if colIDs:
                    tableModel.colIDs = colIDs
                self.resultsTable.sortByColumn(-1, Qt.AscendingOrder)
                self.resultsTable.setModel(tableModel)
                for i in range(tableModel.columnCount()):
                    self.resultsTable.resizeColumnToContents(i)
        except Exception as e:
            log_exception("Error displaying table output")
            self.showStatus(unicode(e))
          
    @pyqtSlot(list, list)
    def showQueryResults(self, classifiers, results):
        cols = [("Rank", self.rankCell),
                ("ID", self.idCell),
                ("Page Title", self.titleCell),
                ("Type", self.typeCell)]
        
        for i, classifierName in enumerate(classifiers):
            cols.append((classifierName, partial(self.categoriesCell, 3 + i)))
        
        tableModel = ResultsTableModel(self, cols)
        rowIDs = []
        for rank, aTuple in enumerate(results):
            row = [rank + 1, convert_string(aTuple[Topology.PAGE_TITLE_INDEX]),
                                         aTuple[Topology.PAGE_NS_INDEX]]
            
            for classification in aTuple[Topology.CATEGORIES_INDEX]:
                row.append(classification)
                
            tableModel.appendContentRow(aTuple[Topology.PAGE_ID_INDEX], row)
            rowIDs.append((aTuple[Topology.PAGE_ID_INDEX],
                           aTuple[Topology.PAGE_NS_INDEX],
                           aTuple[Topology.PAGE_TITLE_INDEX]))
        tableModel.rowIDs = rowIDs
        self.resultsTable.setModel(tableModel)
        self.resultsTable.sortByColumn(0, Qt.AscendingOrder)
        for i in range(tableModel.columnCount()):
            self.resultsTable.resizeColumnToContents(i)
        
    @pyqtSlot(list, int)
    def choosePage(self, candidates, actionID):
        candidateStrings = [self._formatPage(pID, pNs, pTitle) for pTitle, pID, pNs in candidates]
        dialog = ChooseValueDialog(self, "Choose Page", "There are multiple candidates. Please select the page you want to process.", candidateStrings)
        if dialog.exec_() == ChooseValueDialog.Accepted:
            self.performActionOnID(candidates[dialog.selectedIndex][1], candidates[dialog.selectedIndex][2], actionID)
        dialog.deleteLater()
        
    @pyqtSlot(int, int, int)
    def performActionOnID(self, pageID, pageNs, actionID):
        if actionID == self.queryBar.queryActions[QueryBar.S_CATEGORY_PATHS].actionID:
            self._getCategoryPaths.emit(pageID, pageNs)
        elif actionID == self.queryBar.queryActions[QueryBar.S_SIMILAR_ARTICLES].actionID:
            self._getSimilarArticles.emit(pageID)
            
    @pyqtSlot(int)
    def openInBrowser(self, pID):
        webbrowser.open("http://en.wikipedia.org/wiki/index.php?title=-&curid=%d" % pID, new=2, autoraise=True)
    
    def setTitle(self, s):
        if len(s) > 50:
            s = s[:47] + "…"
        self.setWindowTitle(s)
    
    @pyqtSlot(unicode)
    def queryStarted(self, desc):
        self.setTitle(desc)
        self.queryBar.setEnabled(False)
        self.showStatus("Running Query...")
        
    @pyqtSlot(bool)
    def queryFinished(self, success):
        self.queryBar.setEnabled(True)
        if success:
            self.showReady()
        
    def _openNewViewWrapper(self, pID, actionIdx):
        self.openNewView.emit(pID, actionIdx)
        
    def performAction(self, pID, actionIdx):
        self.queryBar.performAction(pID, actionIdx)
        
    @pyqtSlot(QPoint)
    def showContextMenu(self, point):
        index = self.resultsTable.indexAt(point)
        if index != None and self.resultsTable.model() != None:
            popupMenu = QMenu(self)
            with contextlib.closing(self.networkHandler.dbHandler.cursor(prepared=True)) as cursor:
                for anID in self.resultsTable.model().getPageIDs(index):
                    if type(anID) == tuple:
                        pID, pNs, pTitle = anID
                    else:
                        pID = anID
                        pNs, pTitle = self.networkHandler.dbHandler.getNamespaceAndTitle(anID, cursor)
                    pDesc = self._formatPage(pID, pNs, pTitle)
                    popupMenu.addSection(pDesc)
                    headerAction = popupMenu.addAction(pDesc)
                    headerAction.setEnabled(False)
                    for actionIdx, anAction in enumerate(self.queryBar.queryTypes):
                        if not anAction.appearsInContextMenu:
                            continue
                        elif anAction.contextMenuFilter != None:
                            # check if the action should appear
                            if not anAction.contextMenuFilter(pID, pNs, pTitle):
                                continue
                        if anAction.appearsInQueryBar:
                            popupMenu.addAction(anAction.contextMenuString, partial(self._openNewViewWrapper, pID, actionIdx))
                        else:
                            popupMenu.addAction(anAction.contextMenuString, partial(anAction.actionFunc, str(pID), actionIdx))
            popupMenu.exec_(QCursor.pos())
            popupMenu.deleteLater()
            
    def initUI(self):
        centralWidget = QWidget(self)
        centralLayout = QVBoxLayout(centralWidget)
        centralLayout.setContentsMargins(0, 0, 0, 0)
        
        self.queryBar = QueryBar(self, "Query", useTextEdit=True)
        self.queryBar.queryStarted.connect(self.queryStarted)
        self.queryBar.getPages.connect(self.networkHandler.getPages)
        self.queryBar.fullTextSearch.connect(self.networkHandler.fullTextSearch)
        self.queryBar.getEntrycats.connect(self.networkHandler.getEntrycats)
        self.queryBar.getDistances.connect(self.networkHandler.getDistances)
        self.queryBar.preparePageID.connect(self.networkHandler.preparePageID)
        self.queryBar.openInBrowser.connect(self.openInBrowser)
        self.queryBar.classifyPlainText.connect(self.networkHandler.classifyPlainText)
        centralLayout.addWidget(self.queryBar, 0)
        
        self.resultsTable = ResultsTable(self)
        self.resultsTable.setContextMenuPolicy(Qt.CustomContextMenu)
        self.resultsTable.customContextMenuRequested.connect(self.showContextMenu)
        centralLayout.addWidget(self.resultsTable, 1)
        
        self.statusLabel = QLabel(centralWidget) 
        centralLayout.addWidget(self.statusLabel)
        
        self.setWidget(centralWidget)
