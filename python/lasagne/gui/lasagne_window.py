from PyQt5.QtWidgets import QMainWindow, QMenu, QTabWidget, QApplication
from PyQt5.QtGui import QKeySequence
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from lasagne.network.lasagne_client import LasagneClient
from lasagne.gui.lasagne_widget import LasagneWidget

class LasagneWindow(QMainWindow):
    _reconnect = pyqtSignal()
    _checkConnection = pyqtSignal()
    
    def __init__(self, runLocal, lasagneServer, lasagnePort, ovenLogin, delegate):
        super(LasagneWindow, self).__init__()

        self.client = LasagneClient(runLocal, lasagneServer, lasagnePort, ovenLogin, delegate)
            
        self._reconnect.connect(self.client.reconnect)    
        self._checkConnection.connect(self.client.checkConnection)
        
        self.initUI()
        
    def closeEvent(self, closeEvent):
        self.client.close()
        
        QMainWindow.closeEvent(self, closeEvent)   
        
    def reconnect(self):
        self._reconnect.emit()
    
    def checkConnection(self):
        self._checkConnection.emit()
        
    def buildMenuBar(self):
        menuBar = self.menuBar()
        
        lasagneMenu = QMenu("Lasagne", menuBar)
        lasagneMenu.addAction("New Widget", self.newWidget, QKeySequence("CTRL+N"))
        menuBar.addMenu(lasagneMenu)
        
        connMenu = QMenu("Connection", menuBar)
        connMenu.addAction("Check connection", self.checkConnection)
        connMenu.addAction("Reconnect", self.reconnect)
        menuBar.addMenu(connMenu)
        
    def centerOnScreen(self):
        r = self.geometry()
        r.moveCenter(QApplication.desktop().availableGeometry().center())
        self.setGeometry(r)
        
    def newWidget(self):
        newWidget = LasagneWidget(self.client, self)
        newWidget.openNewView.connect(self.openNewView)
        self.addDockWidget(Qt.TopDockWidgetArea, newWidget)
        return newWidget
        
    @pyqtSlot(int, int)
    def openNewView(self, pID, actionIdx):
        newWidget = self.newWidget()
        newWidget.performAction(pID, actionIdx)
        
    def initUI(self):          
        #self.setGeometry(300, 300, 250, 150)
        self.centerOnScreen()
        
        self.setDockNestingEnabled(True)
        self.setTabPosition(Qt.AllDockWidgetAreas, QTabWidget.North)
        
        self.newWidget()
        
        self.buildMenuBar()
        self.checkConnection()
        self.show()
