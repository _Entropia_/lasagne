from PyQt5.QtWidgets import QTreeView
from PyQt5.QtCore import Qt
from lasagne.gui.table_models import TableModelBase

class ResultsTable(QTreeView):
    def __init__(self, parent, sortedColumn = None, ascending = True):
        super(ResultsTable, self).__init__(parent)
        
        self.setSortingEnabled(True)
        self.setHeaderHidden(False)
        self.setAlternatingRowColors(True)
        self.setIndentation(0)
        if sortedColumn != None:
            self.sortByColumn(sortedColumn, Qt.AscendingOrder if ascending else Qt.DescendingOrder)

class ResultsTableModel(TableModelBase):
    PAGE_ID_ROLE = TableModelBase.SORT_ROLE + 1
    
    def __init__(self, dataSource, columns):
        super(ResultsTableModel, self).__init__(dataSource, columns)
        self.rowIDs = None
        self.colIDs = None

    def getPageIDs(self, index):
        result = []
        item = self.itemFromIndex(index)
        cellID = item.data(self.PAGE_ID_ROLE)
        if cellID != None:
            result.append(int(cellID))
        if self.rowIDs != None and self.rowIDs[index.row()] != None:
            result.append(self.rowIDs[index.row()])
        if self.colIDs != None and self.colIDs[index.column()] != None:
            result.append(self.colIDs[index.column()])
            
        return result
        