from PyQt5.QtWidgets import QWidget, QHBoxLayout, QPushButton, QSizePolicy, QComboBox
from PyQt5.QtCore import Qt, QSize, pyqtSignal
from lasagne import convert_string
from lasagne.gui.history_line_edit import HistoryLineEdit, HistoryTextEdit
from lasagne.utils.topology import Topology

class QueryAction(object):
    nextID = 0
    def __init__(self, defaultString, actionFunc, contextMenuString=None, appearsInQueryBar=True, appearsInContextMenu=True, contextMenuFilter=None):
        self.defaultString = defaultString
        self.actionFunc = actionFunc
        self.contextMenuString = contextMenuString if contextMenuString else defaultString
        self.appearsInQueryBar = appearsInQueryBar
        self.appearsInContextMenu = appearsInContextMenu
        self.contextMenuFilter = contextMenuFilter
        self.actionID = QueryAction.nextID
        QueryAction.nextID += 1
    
    def trigger(self, query):
        self.actionFunc(query, self.actionID)

class QueryBar(QWidget):
    PREFERRED_WIDTH = 400
    
    S_FULL_TEXT_SEARCH = u"Full Text"
    S_PAGE_TITLE_SEARCH = u"Page ID / Title"
    S_ENTRY_CATEGORIES = u"Entry Categories"
    S_DISTANCES = u"Distances"
    S_CATEGORY_PATHS = u"Category Paths"
    S_SIMILAR_ARTICLES = u"Similar Articles"
    S_OPEN_IN_BROWSER = u"Open in Browser"
    S_CLASSIFY_PLAIN_TEXT = u"Classify Plain Text"
    
    fullTextSearch = pyqtSignal(unicode)
    getPages = pyqtSignal(unicode)
    getEntrycats = pyqtSignal(unicode)
    getDistances = pyqtSignal(unicode)
    preparePageID = pyqtSignal(unicode, int, int)  # (query, namespace selection, action id)
    openInBrowser = pyqtSignal(int)
    classifyPlainText = pyqtSignal(unicode)
    
    queryStarted = pyqtSignal(unicode)
        
    def __init__(self, parent, buttonText, useTextEdit=False):
        super(QueryBar, self).__init__(parent)
        
        queryBarLayout = QHBoxLayout(self)
        queryBarLayout.setContentsMargins(0, 0, 0, 0)
        
        self.queryTypes = [
                           QueryAction(self.S_FULL_TEXT_SEARCH, lambda query, _id : self.fullTextSearch.emit(query), appearsInContextMenu=False),
                           QueryAction(self.S_PAGE_TITLE_SEARCH, lambda query, _id : self.getPages.emit(query), contextMenuString=u"Details"),
                           QueryAction(self.S_ENTRY_CATEGORIES, lambda query, _id : self.getEntrycats.emit(query), contextMenuFilter=lambda _pID, pNs, _pTitle : pNs == 0),
                           QueryAction(self.S_DISTANCES, lambda query, _id : self.getDistances.emit(query)),
                           QueryAction(self.S_CATEGORY_PATHS, lambda query, anID : self.preparePageID.emit(query, Topology.ACCEPT_ARTICLES | Topology.ACCEPT_CATEGORIES, anID)),
                           QueryAction(self.S_SIMILAR_ARTICLES, lambda query, anID : self.preparePageID.emit(query, Topology.ACCEPT_ARTICLES, anID), contextMenuFilter=lambda _pID, pNs, _pTitle : pNs == 0),
                           QueryAction(self.S_OPEN_IN_BROWSER, lambda query, _anID : self.openInBrowser.emit(int(query)), appearsInQueryBar=False),
                           QueryAction(self.S_CLASSIFY_PLAIN_TEXT, lambda query, _anID : self.classifyPlainText.emit(query), appearsInContextMenu=False)
                           ]
        self.queryActions = {anAction.defaultString : anAction for anAction in self.queryTypes}
    
        self.queryTypeCombo = QComboBox(self)
        for anAction in self.queryTypes:
            self.queryTypeCombo.addItem(anAction.defaultString)
        self.queryTypeCombo.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        queryBarLayout.addWidget(self.queryTypeCombo, 0, Qt.AlignCenter)
        
        if useTextEdit:
            self.entry = HistoryTextEdit(self)
        else:
            self.entry = HistoryLineEdit(self, "Enter Query")
        self.entry.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        queryBarLayout.addWidget(self.entry, 1)
        
        button = QPushButton(buttonText, self)
        button.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.MinimumExpanding)
        queryBarLayout.addWidget(button, 0)
        
        self.entry.returnPressed.connect(self.performQuery)
        button.clicked.connect(self.performQuery)
        
        
    def performAction(self, pID, actionIdx):
        self.queryTypeCombo.setCurrentIndex(actionIdx)
        # self.queryTypes[actionIdx].trigger(unicode(pID))
        self.entry.setText(unicode(pID))
        self.performQuery()
        
    def performQuery(self):
        string = convert_string(self.entry.text())
        
        action = self.queryTypes[self.queryTypeCombo.currentIndex()]
        self.queryStarted.emit(u"%s - %s" % (action.defaultString, string))
        action.trigger(string)
    
    def sizeHint(self):
        sizeHint = QWidget.sizeHint(self)
        return QSize(self.PREFERRED_WIDTH, sizeHint.height())
