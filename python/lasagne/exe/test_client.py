import sys
from lasagne.test.test_case import TestCase
from lasagne import log_error, setLogFile, log_info
from lasagne.network.lasagne_client import LasagneClient
import os
import Pyro4

class TestClient(TestCase):
    def _configOptionParser(self):
        TestCase._configOptionParser(self)
        self.optionParser.add_option("--lasagne-host",
                                     action="store", dest="lasagneHost",
                                     help="Set the Lasagne server hostname (the oven).")
        self.optionParser.add_option("--lasagne-port",
                                     action="store", dest="lasagnePort", default=9090,
                                     help="Set the Lasagne server port.")
        self.optionParser.add_option("--lasagne-login",
                                     action="store", dest="lasagneLogin",
                                     help="Set the user name on the Lasagne server hostname (the oven).")
        self.optionParser.add_option("--local-server", action="store_true",
                                     default=False, dest="local",
                                     help="Connect to a local server, i.e. don't open any SSH tunnels.")
        
    def _runTest(self):
        if not self.options.local:
            if self.options.lasagneHost == None:
                log_error("No host name specified (--lasagne-host)")
                return -1
            if self.options.lasagneLogin == None:
                log_error("No login specified (--lasagne-login)")
                return -1
        
        with LasagneClient(self.options.local, self.options.lasagneHost, self.options.lasagnePort, self.options.lasagneLogin, self.dbHandler) as client:
            client._initializeServer()

            import threading
            
            class TestThread(threading.Thread):
                def __init__(self, uri):
                    super(TestThread, self).__init__()
                    self.server = Pyro4.Proxy(uri)
                    
                def run(self):
                    print "test"
                    self.server.testThreading()
                    print "test finished"
    
            client._initializeServer()
            t1 = TestThread(client.server._pyroUri)
            t2 = TestThread(client.server._pyroUri)
            t1.start()
            t2.start()
            
            t1.join()
            t2.join()
            return 0
            
            topology = client.getTopology()
            for row in topology.getEntrycats(["Banana"]):
                print row
                
            return 0

            query = "air gun fires projectiles compressed gas seismic vortex cannon impact wrench"
            
            setLogFile(os.path.expanduser("~/performance.log"))
            index = client.getLSAIndex()
            
            log_info("Number of documents:", index.size())
            
            log_info("*** Sequential search ***")
            result = index.search(query, parallel=False)
            log_info("Result:", result)
            
            log_info("*** Parallel search ***")
            result = index.search(query)
            log_info("Result:", result)

if __name__ == '__main__':
    sys.exit(TestClient().exitCode)