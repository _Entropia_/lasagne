'''
Analyze various sets of article IDs.
'''

from lasagne.test import TestCase
from lasagne.file.lsa_vectors import openLSAVectorsFile
from lasagne.file import default_paths
import os,sys
import cPickle as pickle
from gensim.utils import smart_open

class AnalyzeArticleSets(TestCase):
    def _configOptionParser(self, _needMySQL=True):
        TestCase._configOptionParser(self, needMySQL=False)
        self.optionParser.add_option("-l","--lsa-vectors",
                  action="store", type="string", dest="lsaVectors", default=default_paths.LSA_VECTORS_FILE,
                  help="Read entry categories from FILE.", metavar="FILE")
        self.optionParser.add_option("--dump-master",
                  action="store_true", dest="dump", default=False,
                  help="Dump master set of article IDs")

    def _getOutputFolderName(self):
        return 'analyze'
        
    def _runTest(self):
        if self.options.lsaVectors == None:
            self.logger.error("LSA Vectors not specified.")
            return False
        
        entryCats = self.entryCats
        categoryDists = self.categoryDists
        lsaVectors = openLSAVectorsFile(self.options.lsaVectors, 'rb')
        
        neededCategories = set(categoryDists.keys())
        self.logger.info("Collecting IDs from LSA Vectors...")
        neededArticles = set()
        for k, _ in lsaVectors:
            neededArticles.add(k)
        self.logger.info("Collecting IDs of unreachable Articles...")
        unreachableArticles = set()
        for k in entryCats.keys():
            v = [cat for cat in entryCats[k] if cat in neededCategories]
            if v == []:
                unreachableArticles.add(k)
        for k in neededArticles:
            if k in entryCats:
                v = [cat for cat in entryCats[k] if cat in neededCategories]
                if v == []:
                    unreachableArticles.add(k)
            else:
                unreachableArticles.add(k)
        
        # Restructuring into dictionary imminent
        self.logger.info("Constructing Sets...")
        sets = {}
        sets['A'] = neededArticles
        sets['B'] = set(entryCats.keys())
        sets['C'] = set(unreachableArticles)
        sets['AiB'] = sets['A'].intersection(sets['B'])
        sets['AiC'] = sets['A'].intersection(sets['C'])
        sets['BiC'] = sets['B'].intersection(sets['C'])
        sets['AiBiC'] = sets['AiB'].intersection(sets['C'])
        sets['AiBdC'] = sets['AiB'].difference(sets['C'])
        sets['AiCdB'] = sets['AiC'].difference(sets['B'])
        sets['BiCdA'] = sets['BiC'].difference(sets['A'])
        sets['AdB'] = sets['A'].difference(sets['B'])
        sets['AdC'] = sets['A'].difference(sets['C'])
        sets['AdBC'] = sets['A'].difference(sets['B'],sets['C'])
        sets['BdA'] = sets['B'].difference(sets['A'])
        sets['BdC'] = sets['B'].difference(sets['C'])
        sets['BdAC'] = sets['B'].difference(sets['A'],sets['C'])
        sets['CdA'] = sets['C'].difference(sets['A'])
        sets['CdB'] = sets['C'].difference(sets['B'])
        sets['CdAB'] = sets['C'].difference(sets['A'],sets['B'])
        sets['AuBuC'] = sets['A'].union(sets['B'],sets['C'])
        
        print "A: LSA Vectors, B: entry Cats, C: unreachable articles"
        for name,s in sets:
            l = len(s)
            if l > 0:
                minV = min(s)
                maxV = max(s)
            else:
                minV = "n/A"
                maxV = "n/A"
            print "|",name,"| =",len(s),"\t| min =",minV,"\t| max =",maxV
        
        if self.options.dump:
            outPath = os.path.join(self.options.outputFolder,"master.pkl")
            self.logger.info("Dumping master set to %s..." % outPath)
            with smart_open(outPath, 'wb') as outFile:
                    pickle.dump(sets['AiBdC'], outFile, protocol=pickle.HIGHEST_PROTOCOL)
        return
        
        
#             
#         with smart_open(fPath, 'wb') as outFile:
#             self.logger.info("Re-Pickling data...")
#             pickler = pickle.Pickler(outFile, protocol=pickle.HIGHEST_PROTOCOL)
#             pickler.dump(data)
    
if __name__ == '__main__':
    sys.exit(AnalyzeArticleSets().exitCode)
