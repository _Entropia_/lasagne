import sys
with open(sys.argv[1], 'rb') as inFile:
    with open(sys.argv[2], 'wb') as outFile:
        outFile.write(inFile.next())
        outFile.write(inFile.next())
        lastIdx = 0
        removed = 0
        for line in inFile:
            line = line.split()
            curIdx = int(line[0])
            if curIdx % 10000 == 0:
                sys.stderr.write("%d documents processed\n")
            if curIdx - lastIdx > 1:
                for i in range(curIdx - lastIdx - 1):
                    print "removing ", lastIdx + i # print 0-based indices
                removed += curIdx - lastIdx - 1
            outFile.write(str(curIdx - removed))
            outFile.write(" ")
            outFile.write(line[1])
            outFile.write(" ")
            outFile.write(line[2])
            outFile.write("\n")
            lastIdx = curIdx
        print "removed", removed, "empty documents"
            