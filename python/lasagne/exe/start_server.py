from lasagne.test import TestCase
import sys
from lasagne.network.lasagne_server import LasagneServer

class StartServer(TestCase):
    def _configOptionParser(self):
        TestCase._configOptionParser(self)
        self.optionParser.add_option("--preload",
                              default=False, dest="preload", action="store_true",
                              help="Pre-load all data.")
    
    def _runTest(self):
        server = LasagneServer("127.0.0.1", 9090, self)
        server.startServer(preload=self.options.preload)
    
    def _getOutputFolderName(self):
        return 'server'

if __name__ == '__main__':
    sys.exit(StartServer().exitCode)
        
