from lasagne.test import TestCase
from gensim.utils import smart_open
import sys
import re
from xml.etree.cElementTree import iterparse
from lasagne.file import default_paths

inst = None

def _get_namespace(tag):
    """Returns the namespace of tag."""
    m = re.match("^{(.*?)}", tag)
    namespace = m.group(1) if m else ""
    if not namespace.startswith("http://www.mediawiki.org/xml/export-"):
        raise ValueError("%s not recognized as MediaWiki dump namespace"
                         % namespace)
    return namespace

def _extract_pages(f, criticalIDs):
    """
    Extract pages from MediaWiki database dump.

    Returns
    -------
    pages : iterable over (str, str)
        Generates (title, content) pairs.
    """
    elems = (elem for _, elem in iterparse(f, events=("end",)))

    # We can't rely on the namespace for database dumps, since it's changed
    # it every time a small modification to the format is made. So, determine
    # those from the first element we find, which will be part of the metadata,
    # and construct element paths.
    elem = next(elems)
    namespace = _get_namespace(elem.tag)
    ns_mapping = {"ns": namespace}
    page_tag = "{%(ns)s}page" % ns_mapping
    text_path = "./{%(ns)s}revision/{%(ns)s}text" % ns_mapping
    page_id_path = "./{%(ns)s}id" % ns_mapping

    for elem in elems:
        if elem.tag == page_tag:
            p_id = elem.find(page_id_path).text
            if p_id in criticalIDs:
                text = elem.find(text_path).text
                print p_id
                print "--------------------------"
                print text.decode('utf-8')
            elem.clear()
           
CRITICAL = """12
40677597
40677648
40677675
40677988
40678004
40678067
40678139
40678186
40678304
40678467
40678449
40678528
40678601
40678819
40678759
40678739
40679170
40679626
40679533
40679756
40679651
40679963
40679790
40680146
40680100
40680066
40679983
40679972
40680331
40680431
40680394
40680380
40680365
40680353
40680343
40680455"""
                
class TestClient(TestCase):
    def _configOptionParser(self):
        TestCase._configOptionParser(self)
    
    def _runTest(self):
        critical = set()
        for pID in CRITICAL:
            critical.add(pID.strip())
        
        with smart_open(default_paths.WIKI_XML_DUMP_FILE, 'rb') as wikiDump:
            _extract_pages(wikiDump, critical)
    
if __name__ == '__main__':
    sys.exit(TestClient().exitCode)
        
