from lasagne.test import TestCase
from lasagne import BASE_CATEGORIES
import sys, os, cmd, traceback
import shlex
from lasagne.utils.topology import Topology

class GetBaseCategoriesTest(TestCase, cmd.Cmd):
    class CatPath(object):
        def __init__(self, cat, prev):
            self.cat = cat
            self.prev = prev
    
    def __init__(self):
        self.titleCache = {}
        self.topology = Topology(self)
        cmd.Cmd.__init__(self)
        TestCase.__init__(self)
    
    def _configOptionParser(self):
        super(GetBaseCategoriesTest, self)._configOptionParser()
    
    def _articleDescription(self, _p_title, p_id, p_ns, cursor=None):
        realTitle = self.getPageName(p_id, cursor)
        if p_ns == 0:
            return realTitle + " (article)"
        elif p_ns == 14:
            return realTitle + " (cat)"
        else:
            return None
    
    def _shouldFollow(self, parcat, baseCatPath, distances, pathIndices, finishedPaths):
        if not parcat in self.categoryDists:
            return False
        curDistances = self.categoryDists[parcat]
        for idx in pathIndices - set(finishedPaths.keys()):
            if curDistances[idx] == 1:
                # finished minimum path to this category
                baseCatPath = self.CatPath(self.baseCategories[idx], self.CatPath(parcat, baseCatPath))
                if idx in finishedPaths:
                    finishedPaths[idx].append(baseCatPath)
                else:
                    finishedPaths[idx] = [baseCatPath]
            elif curDistances[idx] <= distances[idx]:
                return True
        return False
        
    def _minDist(self, dists):
        realDists = [dist for dist in dists if dist >= 0]
        return min(realDists) if realDists else -1
    
    def getDistances(self, pID):
        if pID in self.categoryDists:
            return self.categoryDists[pID]
        elif pID in self.entryCats:
            distanceVectors = [self.categoryDists[entryCat] for entryCat in self.entryCats[pID] if entryCat in self.categoryDists]
            # check if we already are at a base category
            add_vector = [-1] * len(BASE_CATEGORIES)
            for entryCat in self.entryCats[pID]:
                for i, baseCat in enumerate(self.baseCategories):
                    if entryCat == baseCat:
                        add_vector[i] = 0
            distanceVectors.append(add_vector)
            return map(self._minDist, zip(*distanceVectors))
    
    def _printTableOutput(self, output):
        if len(output) > 1:
            self.appendOutput(*output.pop(0))
            self.appendSeparator('-')
        for row in output:
            self.appendOutput(*row)
        
        self.flushOutput()

    def do_entrycats(self, args):
        """Prints the entry categories for articles.
usage: entrycats article1 [article2 [article 3 ...]]"""
        args = shlex.split(args)
        if not args:
            self.do_help("entrycats")

        result = self.topology.getEntrycats(args)
        self._printTableOutput(result)
    
    def do_path(self, args):
        """ Computes and prints the minimum path from an article to the base categories.
usage: path article [category1 [category2 ...]]"""
        args = shlex.split(args)
        if len(args) < 1:
            self.do_help("path")
        try:
            title = args.pop(0)
            try:
                intVal = int(title)
                title = intVal
            except:
                pass
            candidates = self.topology.getCandidates(title, Topology.ACCEPT_ARTICLES | Topology.ACCEPT_CATEGORIES)
            
            if len(candidates) == 0:
                print "No candidates found."
                return
            elif len(candidates) == 1:
                i = 0
            else:
                print "There are multiple articles with the title '%s':" % title
                for i, row in enumerate(candidates):
                    description = self._articleDescription(title, row[0], row[1])
                    if description:
                        print "(%d): %s" % (i, description)
                i = -1
                while (True):
                    try:
                        out = sys.stdout
                        out.write("Enter the article number to be processed (0): ")
                        line = sys.stdin.readline().strip()
                        if not line:
                            i = 0
                            break
                        i = int(line)
                        break
                    except:
                        continue
                
            if i == -1:
                return
            
            result = self.topology.getCategoryPaths(candidates[i][0], candidates[i][1], args if args else None)
            self._printTableOutput(result)
        except:
            traceback.print_exc()
    
    def do_distances(self, titles, printRes=True):
        """Computes the minimum distances from articles to the base categories.
usage: distances article1 [article2 [article3 ...]]"""
        titles = shlex.split(titles)
        allResults = []
        for aTitle in titles:
            ids = self.topology.getIDs(aTitle)
            
            results = []
            for pID, pNamespace in ids:
                name = self._articleDescription(aTitle, pID, pNamespace, None)
                if name == None:
                    continue
                distances = self.getDistances(pID)
                if distances != None:
                    results.append((name, distances))
            if not results:
                print "No article found with the name '%s'." % aTitle
            else:
                allResults.extend(results)
                    
        if printRes:
            self.appendOutput("", *BASE_CATEGORIES)
            self.appendSeparator('-')
            
        for name, distances in allResults:
            self.appendOutput(name, *distances)
            
        if printRes:
            self.flushOutput()
    
    def do_exit(self, _):
        return True
    
    def _runTest(self):
        if not os.path.exists(self.options.categoryDict):
            self.logger.error("Category dictionary file %s does not exist." % self.options.categoryDict)
            sys.exit(-1)
        
        self.logger.info("Fetching page IDs for base categories")
        self.initializeBaseCategories()
        
        import readline
        import rlcompleter
    
        if 'libedit' in readline.__doc__:
            readline.parse_and_bind("bind ^I rl_complete")
        else:
            readline.parse_and_bind("tab: complete")
    
        self.cmdloop("Welcome. Let 'help' be your guide.")

if __name__ == '__main__':
    sys.exit(GetBaseCategoriesTest().exitCode)
    
    
