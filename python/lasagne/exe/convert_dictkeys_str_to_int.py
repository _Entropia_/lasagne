import sys, os
from lasagne.test import TestCase
import cPickle as pickle
from gensim.utils import smart_open

class ConvertDictKeys(TestCase):
    def _configOptionParser(self, _needMySQL=True):
        TestCase._configOptionParser(self, needMySQL=False)
        
    def _runTest(self):
        if not self.args:
            self.logger.error("No file specified.")
            return False
        
        fPath = self.args[0]
        if not os.path.exists(fPath):
            self.logger.error("File %s does not exist." % fPath)
            return False
        
        with smart_open(fPath, 'rb') as inFile:
            self.logger.info("Unpickling %s..." % fPath)
            dict = pickle.load(inFile)
            
        os.rename(fPath, fPath + ".bak")
        
        self.logger.info("Converting...")
        dictNew = {}
        
        for k in dict.keys():
            v = dict[k]
            dictNew[int(k)]=v
            
        if len(dict) != len(dictNew):
            self.logger.error("Lenghts of dicts differ!")
            return False
            
        with smart_open(fPath, 'wb') as outFile:
            self.logger.info("Re-Pickling data...")
            pickler = pickle.Pickler(outFile, protocol=pickle.HIGHEST_PROTOCOL)
            pickler.dump(dictNew)
    
if __name__ == '__main__':
    sys.exit(ConvertDictKeys().exitCode)
