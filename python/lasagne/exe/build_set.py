'''
Convert category dict from distance to 0/1 values
then combine as needed to build new dictionary
with pageIDs as keys and 0/1 category vectors.
Final result should be:
<pageID> <documentVector> <categoryVector>
where <documentVector> is the LSI-representation
and <categoryVector> is the combined 0/1 vector
which assigns base categories to the article.
'''

import os
import sys
import random
from lasagne import BASE_CATEGORIES
from lasagne.test import TestCase
from lasagne.classify.categorizer import Categorizer
import datetime
import numpy  # needed for mean and median
import cPickle as pickle
from gensim.utils import smart_open

def invert(v):
    res = []
    for e in v:
        if e == 0:
            res.append(1)
        elif e == 1:
            res.append(0)
    return res

class BuildSet(TestCase):
    def _configOptionParser(self):
        TestCase._configOptionParser(self, needMySQL=False)
        self.optionParser.add_option("--master",
                  action="store", type="string", dest="master", default=None,
                  help="Load master set of article IDs from FILE", metavar="FILE")
        self.optionParser.add_option("-M", "--mode",
                  action="store", type="int", dest="mode", default=0,
                  help="Set mode. 0: convert, then combine | 1: combine, then convert")
        self.optionParser.add_option("-m", "--margin",
                  action="store", type="int", dest="margin", default=0,
                  help="Set margin which is added to the minimum distance for classification")
        self.optionParser.add_option("-s", "--selection-style",
                  action="store", type="int", dest="style", default=0,
                  help="Set selection style. 0 (default): proportional, 1: balanced, 2: double-balanced")
        
        self.optionParser.add_option("--frac",
                  action="store", type="float", dest="frac", default=0.10,
                  help="Set fraction of representatives per category to be included in set. [only relevant with selection-style=0]")
        self.optionParser.add_option("--num",
                  action="store", type="int", dest="num", default=1000,
                  help="Set number of representatives for all categories to be included in set. [only relevant with selection-style=1 or 2]. In case of double-balanced, num also specifies the number of counter-examples.")
            
    def statistics(self, dictionary):
        s = []
        d = {}
        probe = {}
        for v in range(0, len(BASE_CATEGORIES) + 1):
            d[v] = 0
            probe[v] = []
        for key, vec in dictionary.iteritems():
            s.append(sum(vec))
            if len(probe[sum(vec)]) < 9:
                probe[sum(vec)].append(key)
        self.logger.info("Distribution of 1s:")
        self.logger.info("Min: %d | Mean: %d | Med: %d | Max: %d" % (min(s), numpy.mean(s), numpy.median(s), max(s)))
        for v in s:
            d[v] += 1
        for i in d.iteritems():
            print i
        for i in probe.iteritems():
            print i
    
    def _runTest(self):
        startTime = datetime.datetime.now()
        
        outPath = os.path.join(self.options.outputFolder, self._getOutputFolderName())
        if os.path.exists(outPath + ".pkl"):
            self.logger.error("Output file already exists.")
            return False
        if self.options.master == None:
            self.logger.error("Master set not specified.")
            return False
        
        self.logger.info("Loading Master Set from %s" % self.options.master)
        with smart_open(self.options.master, 'rb') as inFile:
            master = pickle.load(inFile)
        
        self.logger.info("Initializing classifier")
        categorizer = Categorizer(entryCats=self.options.entryCatDict,
                                 categoryDists=self.options.categoryDict,
                                 mode=self.options.mode,
                                 margin=self.options.margin)
                
        self.logger.info("Classifying articles")
        classified = categorizer.classify(master)
        
        totalReps = map(sum, zip(*classified.values()))
        self.logger.info("Total number of representatives per category:")
        self.logger.info("%s" % str(totalReps))
        catReps = [0] * len(BASE_CATEGORIES)  # number of category representatives already chosen
        chosen = set()
        keys = classified.keys()
        random.shuffle(keys)
        
        self.logger.info("Building set")
        if self.options.style == 0:        
            frac = self.options.frac
            minReps = map(lambda x: int(x * frac * 0.9), totalReps)
            maxReps = map(lambda x: int(x * frac * 1.1), totalReps)
            
            complete = False
            for art in keys:
                # build preliminary representatives
                prelReps = map(sum, zip(catReps, classified[art]))
                # check if any category exceeds max
                accept = True
                for r, m in zip(prelReps, maxReps):
                    if r > m:
                        accept = False
                        break
                # if art fits into set, put it in
                if accept:
                    chosen.add(art)
                    catReps = prelReps  # update number of representatives
                # check if we are done
                complete = True
                for r, m in zip(catReps, minReps):
                    if r < m:
                        complete = False
                        break
                if complete:
                    break
        if (self.options.style == 1 or self.options.style == 2):
            num = self.options.num
            # determine order in which to fill categories (form lowest number of total representatives to highest)
            order = [(v, i) for v, i in zip(totalReps, range(len(BASE_CATEGORIES)))]
            order.sort(key=lambda t: t[0])
            order = [t[1] for t in order]
            for i in order:  # go through all BASE_CATEGORIES in order of their representatives
                if catReps[i] >= num:  # if this category is already filled, skip it
                    continue
                for art in keys:  # otherwise, go through all articles
                    if (classified[art][i] == 1 and catReps[i] < num):  # if this article is in the desired category and there is still room
                        catReps = map(sum, zip(catReps, classified[art]))
                        chosen.add(art)  # add it
        if self.options.style == 2:  # double balanced
            catConReps = [0] * len(BASE_CATEGORIES)  # number of category counter-representatives already chosen
            # catConReps = [len(chosen) - catReps[i] for i in range(len(BASE_CATEGORIES))] # include counter-examples already chosen
            for i in reversed(order):
                if catConReps[i] >= num:  # if this category already has enough counter-examples, skip it
                    continue
                for art in reversed(keys):  # go through all articles (in reversed order to reduce duplicates)
                    if (classified[art][i] == 0 and catConReps[i] < num):
                        con = invert(classified[art])  # invert vector, since we are counting counter-examples
                        catConReps = map(sum, zip(catConReps, con))
                        chosen.add(art)
        self.logger.info("%d articles chosen for set. Category distribution:" % len(chosen))
        self.logger.info("%s" % str(catReps))
        catConReps = [len(chosen) - catReps[i] for i in range(len(BASE_CATEGORIES))]
        factions = [float(catReps[i]) / float(catConReps[i]) for i in range(len(BASE_CATEGORIES))]
        factions = [round(f, 2) for f in factions]
        self.logger.info("Counter-example distribution:")
        self.logger.info("%s" % str(catConReps))
        self.logger.info("Ratios of examples:counter-examples:")
        self.logger.info("%s" % str(factions))
        
        outPath = outPath + ".pkl"
        self.logger.info("Saving Set to %s" % outPath)
        with smart_open(outPath, 'wb') as outFile:
            pickle.dump(chosen, outFile, protocol=pickle.HIGHEST_PROTOCOL)
                                
        endTime = datetime.datetime.now()
        self.logger.info("Consumed time: %s sec" % (endTime - startTime).total_seconds())
        
    def _getOutputFolderName(self):
        return 'set'

if __name__ == '__main__':
    sys.exit(BuildSet().exitCode)
