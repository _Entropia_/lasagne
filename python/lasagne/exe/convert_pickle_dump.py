from lasagne.test import TestCase
import cPickle as pickle
import sys, os
from gensim.utils import smart_open

class ConvertPickleDump(TestCase):
    def _configOptionParser(self, _needMySQL=True):
        TestCase._configOptionParser(self, needMySQL=False)
        
    def _runTest(self):
        if not self.args:
            self.logger.error("No file specified.")
            return False
        
        fPath = self.args[0]
        if not os.path.exists(fPath):
            self.logger.error("File %s does not exist." % fPath)
            return False
        
        with smart_open(fPath, 'rb') as inFile:
            self.logger.info("Unpickling %s..." % fPath)
            data = pickle.load(inFile)
            
        os.rename(fPath, fPath + ".bak")
            
        with smart_open(fPath, 'wb') as outFile:
            self.logger.info("Re-Pickling data...")
            pickler = pickle.Pickler(outFile, protocol=pickle.HIGHEST_PROTOCOL)
            pickler.dump(data)
    
if __name__ == '__main__':
    sys.exit(ConvertPickleDump().exitCode)
