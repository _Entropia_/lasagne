from lasagne import log_info
from gensim.utils import smart_open
import cPickle as pickle
import sys
import random
from pybrain.tools.shortcuts import buildNetwork
from pybrain.structure import TanhLayer
from pybrain.structure import SigmoidLayer
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
from lasagne.test.run_classifier import TrainClassifier
from lasagne.classify.brain import Brain

"Note to self: try learningRate=0.3, momentum=0.6"

"IMPORTANT NOTE: arac Networks CANNOT BE PICKLED - use pyBrain networks and copy params"

"totalReps = map(sum, zip(*ds['target'])) works, but needs converting to int afterwards"

class ModSupervisedDataSet(SupervisedDataSet):
    def splitWithProportion(self, proportion=0.5):
        """Produce two new datasets, the second one containing the fraction
        of category representatives given by `proportion` of the samples."""
        leftDs = self.copy()
        leftDs.clear()
        rightDs = leftDs.copy()
        
        rightIndices = set()
        
        cats = self['target'].astype(int).tolist()
        
        # calculate representative vectors for rightDs
        totalReps = map(sum, zip(*cats))
        minReps = map(lambda x: int(x * proportion * 0.9), totalReps)
        maxReps = map(lambda x: int(x * proportion * 1.1), totalReps)
                
        complete = False
        catReps = [0] * len(totalReps)  # number of category representatives already chosen
        keys = range(len(self))
        random.shuffle(keys)
        for k in keys:
            # build preliminary representatives
            prelReps = map(sum, zip(catReps, cats[k]))
            # check if any category exceeds max
            accept = True
            for r, m in zip(prelReps, maxReps):
                if r > m:
                    accept = False
                    break
            # if art fits into set, put it in
            if accept:
                rightIndices.add(k)
                catReps = prelReps  # update number of representatives
            # check if we are done
            complete = True
            for r, m in zip(catReps, minReps):
                if r < m:
                    complete = False
                    break
            if complete:
                break
        
        index = 0
        for sp in self:
            if index in rightIndices:
                rightDs.addSample(*sp)
            else:
                leftDs.addSample(*sp)
            index += 1
        return leftDs, rightDs

class ModBackpropTrainer(BackpropTrainer):
    def __init__(self, net, outPath, fNet, dataset=None, learningrate=0.01, lrdecay=1.0,
                 momentum=0., verbose=False, batchlearning=False,
                 weightdecay=0.):
        super(ModBackpropTrainer, self).__init__(fNet if fNet != None else net, dataset, learningrate, lrdecay, momentum, verbose, batchlearning, weightdecay)
        self.net = net
        self.outPath = outPath
    
    def trainUntilConvergence(self, dataset=None, maxEpochs=None, verbose=None,
                              continueEpochs=10, validationProportion=0.25):
        log_info("Training Network")
        epochs = 0
        if dataset == None:
            dataset = self.ds
        if verbose == None:
            verbose = self.verbose
        # Split the dataset proportionally: validationProportion of the samples for 
        # validation.
        trainingData, validationData = (
            dataset.splitWithProportion(validationProportion))
        if not (len(trainingData) > 0 and len(validationData)):
            raise ValueError("Provided dataset too small to be split into training " + 
                             "and validation sets with proportion " + str(validationProportion))
        log_info("%d samples for training, %d samples for validation" % (len(trainingData), len(validationData)))
        self.ds = trainingData
        bestweights = self.module.params.copy()
        bestverr = self.testOnData(validationData)
        log_info("Initial validation error: %g" % bestverr)
        validationErrors = [bestverr]
        while True:
            self.train()
            validationErrors.append(self.testOnData(validationData))
            if epochs == 0 or validationErrors[-1] < bestverr:
                # one update is always done
                bestverr = validationErrors[-1]
                bestweights = self.module.params.copy()
            
            if maxEpochs != None and epochs >= maxEpochs:
                self.module.params[:] = bestweights
                break
            epochs += 1
            
            if len(validationErrors) >= continueEpochs * 2:
                # have the validation errors started going up again?
                # compare the average of the last few to the previous few
                old = validationErrors[-continueEpochs * 2:-continueEpochs]
                new = validationErrors[-continueEpochs:]
                if min(new) > max(old):
                    self.module.params[:] = bestweights
                    break
            # Limit size of history
            validationErrors = validationErrors[-continueEpochs * 2:]
            
            if epochs % 10 == 0:
                log_info("%d epochs trained. Best validation error: %g" % (epochs, bestverr))
                log_info("Saving Neural Net to %s..." % self.outPath)
                # get trained params back
                self.net.params[:] = bestweights
                with smart_open(self.outPath, 'wb') as outFile:
                    pickle.dump(self.net, outFile, protocol=pickle.HIGHEST_PROTOCOL)

        self.ds = dataset
        log_info("Training complete after %d epochs. Best validation error: %g" % (epochs, bestverr))
        return

class RunNetwork(TrainClassifier):
    def _configOptionParser(self):
        TrainClassifier._configOptionParser(self, needMySQL=False)
        self.optionParser.add_option("-p", "--validProp",
                  action="store", type="float", dest="validProp", default=0.25,
                  help="Set proportion of Training Set to be used for validation. [Default: 0.25]")
        self.optionParser.add_option("-H", "--hidden-neurons",
                  action="store", type="int", dest="hiddenNeurons", default=260,
                  help="Set number of hidden neurons to N. [Only relevant if network is not read from file]", metavar="N")
        self.optionParser.add_option("-r", "--learning-rate",
                  action="store", type="float", dest="learningRate", default=0.01,
                  help="Set learning rate. [Default: 0.01]")
        self.optionParser.add_option("-m", "--momentum",
                  action="store", type="float", dest="momentum", default=0.0,
                  help="Set momentum. [Default: 0.0]")
        
        # Transfer Functions (binary choice)
        self.optionParser.add_option("-S", "--sigmoid",
                  action="store_true", dest="sigmoid", default=True,
                  help="Use Sigmoid squashing function. [Default]")
        self.optionParser.add_option("-T", "--tanh",
                  action="store_false", dest="sigmoid",
                  help="Use Tanh squashing function.")

    def _getClassificationDelegate(self):
        return self
    
    def createTrainingSet(self, _size, vecLen, numCats):
        return ModSupervisedDataSet(vecLen, numCats)

    def _getFastBrain(self):
        # net is now a pyBrain Network, which we'll keep for pickling.
        # But we want to use an arac Net for actually doing stuff, so ...
        self.fNet = self.net.convertToFastNetwork()
        return Brain(self.fNet if self.fNet != None else self.net)

    def saveClassifier(self, _classifier, path):
        # get trained params back
        if self.fNet != None:
            self.net.params[:] = self.fNet.params.copy()
        with smart_open(path, 'wb') as outFile:
            pickle.dump(self.net, outFile, protocol=pickle.HIGHEST_PROTOCOL)
    
    def loadClassifier(self, path):
        self.outPath = path
        with smart_open(path, 'rb') as inFile:
            self.net = pickle.load(inFile)
        return self._getFastBrain()
    
    def createClassifier(self, inputSize, outputSize, outPath):
        self.outPath = outPath
        
        if(not self.options.sigmoid):
            hc = TanhLayer
        else:
            hc = SigmoidLayer
        self.logger.info("Creating new Neural Network with <%d,%d,%d> neurons" % (inputSize, self.options.hiddenNeurons, outputSize))
        self.net = buildNetwork(inputSize, self.options.hiddenNeurons, outputSize, hiddenclass=hc)
        return self._getFastBrain()

    def train(self, _classifier, trainingSet):
        # Create Trainer and train net
        trainer = ModBackpropTrainer(self.net, self.outPath, self.fNet, trainingSet,
                                     learningrate=self.options.learningRate,
                                     momentum=self.options.momentum, verbose=False)
        trainer.trainUntilConvergence(validationProportion=self.options.validProp)

    def _classifierName(self):
        return "net"

if __name__ == '__main__':
    sys.exit(RunNetwork().exitCode)
