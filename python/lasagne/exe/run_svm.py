from gensim.utils import smart_open
import cPickle as pickle
import sys
import numpy
from sklearn.svm import LinearSVC
from lasagne.test.run_classifier import TrainClassifier
from lasagne import log_info
import multiprocessing

class SVMTrainingSet(object):
    def __init__(self, size, vecLen, numCats):
        self.size = 0
        self.vectors = numpy.empty((size, vecLen), dtype=numpy.float32)
        self.classes = [numpy.empty(size, dtype=numpy.bool8) for _ in xrange(numCats)]
    def addSample(self, vec, classes):
        self.vectors[self.size] = vec
        for i, label in enumerate(classes):
            self.classes[i][self.size] = label
        self.size += 1
    def __iter__(self):
        for i, vec in enumerate(self.vectors):
            classes = [classes[i] for classes in self.classes]
            yield vec, classes
    def __len__(self):
        return self.size

class SVMClassifier(object):
    def __init__(self, path):
        with smart_open(path, 'rb') as inFile:
            self.svms = pickle.load(inFile)
        
    def classify(self, vec, picklable = True):
        vec = [svm.predict([vec])[0] for svm in self.svms]
        if picklable:
            vec = [bool(val) for val in vec]
        return vec

inst = None

def fitSVMWrapper(i):
    global inst
    return inst.fitSVM(i)

class RunSVM(TrainClassifier):
    #def _configOptionParser(self):
    #    TrainClassifier._configOptionParser(self, needMySQL=False)

    def _getClassificationDelegate(self):
        return self
    
    def createTrainingSet(self, size, vecLen, numCats):
        return SVMTrainingSet(size, vecLen, numCats)

    def saveClassifier(self, classifier, path):
        with smart_open(path, 'wb') as outFile:
            pickle.dump(classifier.svms, outFile, protocol=pickle.HIGHEST_PROTOCOL)
    
    def loadClassifier(self, path):
        return SVMClassifier(path)
    
    def createClassifier(self, _vecLen, numCats, _outPath):
        svms = [LinearSVC()]*numCats # TODO params?
        return SVMClassifier(svms)

    def fitSVM(self, i):
        log_info("Fitting SVM %d/%d..." % (i + 1, len(self.classifier.svms)))
        svm = self.classifier.svms[i]
        vectors = self.trainingSet.vectors
        labels = self.trainingSet.classes[i]
        svm.fit(vectors, labels)
        return svm

    def train(self, classifier, trainingSet):
        global inst
        inst = self
        self.classifier = classifier
        self.trainingSet = trainingSet
        
        processes = max(1, min(len(classifier.svms), multiprocessing.cpu_count()))
        log_info("Starting %d processes" % processes)
        pool = multiprocessing.Pool(processes)
        
        classifier.svms = pool.map(fitSVMWrapper, xrange(len(classifier.svms)))
        pool.terminate()

    def _classifierName(self):
        return "svm"

if __name__ == '__main__':
    sys.exit(RunSVM().exitCode)
