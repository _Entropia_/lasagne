from lasagne.test import TestCase

from xml.etree.cElementTree import iterparse  # LXML isn't faster, so let's go with the built-in solution
from lasagne.wiki.alt_wikicorpus import _get_namespace, ARTICLE_MIN_WORDS
from gensim.utils import smart_open
from gensim import utils

import os, sys, re, multiprocessing
import cPickle as pickle
import contextlib

inst = None

def processTupleWrapper(group):
    global inst
    return inst.processTuple(group)

class ExtractArticles(TestCase):
    def _configOptionParser(self):
        TestCase._configOptionParser(self)

    def _getOutputFolderName(self):
        return "entrycats"

    def _runTest(self):
        if not self.args:
            self.logger.error("No input file specified.")
            return False
        
        inPath = self.args[0]
        if not os.path.exists(inPath):
            self.logger.error("ERROR: input file %s does not exist." % inPath)
            return False
        
        outPath = os.path.join(self.options.outputFolder, "entrycats.pkl.bz2")
        entryCats = {}
        
        self.initMySQL()
        
        for i, tup in enumerate(self.extract(smart_open(inPath))):
            p_id, cats = tup
            entryCats[p_id] = cats
            if i % 10000 == 0:
                self.logger.info("%d articles processed" % i)

        with smart_open(outPath, 'wb') as outFile:
            pickle.dump(entryCats, outFile, protocol=pickle.HIGHEST_PROTOCOL)
    
    def _extract_pages(self, f):
        elems = (elem for _, elem in iterparse(f, events=("end",)))
    
        # We can't rely on the namespace for database dumps, since it's changed
        # it every time a small modification to the format is made. So, determine
        # those from the first element we find, which will be part of the metadata,
        # and construct element paths.
        elem = next(elems)
        namespace = _get_namespace(elem.tag)
        ns_mapping = {"ns": namespace}
        page_tag = "{%(ns)s}page" % ns_mapping
        ns_path = "./{%(ns)s}ns" % ns_mapping
        id_path = "./{%(ns)s}id" % ns_mapping
        text_path = "./{%(ns)s}revision/{%(ns)s}text" % ns_mapping
        # title_path = "./{%(ns)s}title" % ns_mapping
    
        for elem in elems:
            if elem.tag == page_tag:
                # title = elem.find(title_path).text
                text = elem.find(text_path).text
                p_ns = elem.find(ns_path).text
                p_id = int(elem.find(id_path).text)
                
                if p_ns == "0":
                    yield p_id, text
    
                # Prune the element tree, as per
                # http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
                # except that we don't need to prune backlinks from the parent
                # because we don't use LXML.
                # We do this only for <page>s, since we need to inspect the
                # ./revision/text element. The pages comprise the bulk of the
                # file, so in practice we prune away enough.
                elem.clear()
      
    def numWords(self, string):
        numWords = 0
        for _ in re.finditer('\w+', string):
            numWords += 1
            if numWords >= ARTICLE_MIN_WORDS:
                break
        return numWords
    
    def processTuple(self, group):
        result = []
        try:
            with contextlib.closing(self.newConnection()) as cnx:
                with contextlib.closing(cnx.cursor(prepared=True)) as cursor:
                    for p_id, p_text in group:
                        if p_text != None and len(p_text) > 0:# and self.numWords(p_text) >= ARTICLE_MIN_WORDS:
                            result.append((p_id, self.getArticleParentcats(p_id, cursor)))
        except:
            self.logger.exception("Exception during article processing.")
        return result
                
    def extract(self, f):
        global inst
        inst = self
        
        texts = self._extract_pages(f)
        processes = max(1, multiprocessing.cpu_count())
        self.logger.info("starting %d processes" % processes)
        pool = multiprocessing.Pool(processes)
        
        chunks = utils.chunkize(texts, chunksize=1000, maxsize=1)
            
        for group in pool.imap(processTupleWrapper, chunks):
            for p_id, cats in group:
                if cats == None:
                    continue
                else:
                    yield p_id, cats
        pool.terminate()
    
if __name__ == '__main__':
    sys.exit(ExtractArticles().exitCode)
