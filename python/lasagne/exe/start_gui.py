from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QFont
from PyQt5.QtCore import QCoreApplication, QSysInfo
from lasagne.gui.lasagne_window import LasagneWindow
import sys
from lasagne.test.test_case import TestCase
from lasagne import log_error
import signal

class StartGUI(TestCase):
    def _configOptionParser(self):
        TestCase._configOptionParser(self)
        self.optionParser.add_option("--lasagne-host",
                                     action="store", dest="lasagneHost",
                                     help="Set the Lasagne server hostname (the oven).")
        self.optionParser.add_option("--lasagne-port",
                                     action="store", dest="lasagnePort", default=9090,
                                     help="Set the Lasagne server port.")
        self.optionParser.add_option("--lasagne-login",
                                     action="store", dest="lasagneLogin",
                                     help="Set the user name on the Lasagne server hostname (the oven).")
        self.optionParser.add_option("--local-server", action="store_true",
                                     default=False, dest="local",
                                     help="Connect to a local server, i.e. don't open any SSH tunnels.")
        
    def handleInterrupt(self, _signal, _frame):
        self.window.close()
        QCoreApplication.exit(0)
    
    def _runTest(self):
        if not self.options.local:
            if self.options.lasagneHost == None:
                log_error("No host name specified (--lasagne-host)")
                return -1
            if self.options.lasagneLogin == None:
                log_error("No login specified (--lasagne-login)")
                return -1
        
        signal.signal(signal.SIGINT, self.handleInterrupt)
        
        #mysqlPass = getpass.getpass("Enter password for MySQL user '%s': " % self.options.user)
        app = QApplication(sys.argv)
        self.window = LasagneWindow(self.options.local,
                                    self.options.lasagneHost,
                                    self.options.lasagnePort,
                                    self.options.lasagneLogin,
                                    self)
        self.window.raise_()
        return app.exec_()
    
    def _getOutputFolderName(self):
        return 'gui'

if __name__ == '__main__':
    sys.exit(StartGUI().exitCode)