from lasagne.test import TestCase

from gensim.models import TfidfModel, LsiModel, LdaModel
from lasagne.wiki.alt_wikicorpus import WikiCorpus
from gensim.corpora import Dictionary
import os
from gensim.utils import smart_open
import sys
from gensim.corpora.mmcorpus import MmCorpus
import cPickle as pickle
import itertools
from lasagne.file.lsa_vectors import openLSAVectorsFile
from lasagne import log_warning

class BuildDocumentVectors(TestCase):
    DEFAULT_WORDS_TO_KEEP       = 200000
    VECTOR_LENGTH               = 400
    TOKEN_FREQUENCY_LIMIT       = 0.1
    TOKEN_FREQUENCY_LOWER_BOUND = 20

    def _configOptionParser(self):
        super(BuildDocumentVectors, self)._configOptionParser(needMySQL=False)
        self.optionParser.add_option("-w", "--words-to-keep",
                          action="store", type="int", dest="keepWords", default=self.DEFAULT_WORDS_TO_KEEP,
                          help="Controls how many of the most frequent words to keep (after removing tokens according to bounds).")
        self.optionParser.add_option("--token_frequency_limit",
                          action="store", type="float", dest="freqLimit", default=self.TOKEN_FREQUENCY_LIMIT,
                          help="Specifies an upper bound for the relative frequency of a term.")
        self.optionParser.add_option("--token_frequency_lower_bound",
                          action="store", type="int", dest="freqLowerBound", default=self.TOKEN_FREQUENCY_LOWER_BOUND,
                          help="Specifies an lower bound for the absolute frequency of a term.")
        self.optionParser.add_option("--vector-length",
                          action="store", type="int", dest="vectorLength", default=self.VECTOR_LENGTH,
                          help="Sets the LSA vector length.")
        self.optionParser.add_option('-l', "--lemmatize",
                          default=False, dest="lemmatize", action="store_true",
                          help="Enable lemmatizing terms.")

    def _getOutputFolderName(self):
        return "vectors"

    def _runTest(self):
        if not self.args:
            self.logger.error("No input file specified.")
            return False
        
        inPath = self.args[0]
        if not os.path.exists(inPath):
            self.logger.error("Input file %s does not exist." % inPath)
            return False
            
        outputBase = os.path.join(self.options.outputFolder, "analysis_")
        vectorLength = self.options.vectorLength
        keepWords = self.options.keepWords
        freqLimit = self.options.freqLimit
        freqLowerBound = self.options.freqLowerBound
        
        tfidfPath = None
        if os.path.exists(outputBase + 'tfidf.mm'):
            tfidfPath = outputBase + 'tfidf.mm'
        elif os.path.exists(outputBase + 'tfidf.mm.bz2'):
            tfidfPath = outputBase + 'tfidf.mm.bz2'
        
        wiki = None
        
        if not os.path.exists(outputBase + "pageids.pkl") or\
                not os.path.exists(outputBase + "wordids.pkl.bz2") or \
                tfidfPath == None:
            self.logger.info("*** Creating Wikipedia corpus...")
            wiki = WikiCorpus(inPath, lemmatize=self.options.lemmatize)
        
            pageIDs = wiki.pageIDs
            dictionary = wiki.dictionary
            
            self.logger.info("*** Saving Page IDs...");
            pickle.dump(pageIDs, smart_open(outputBase + "pageids.pkl", 'wb'), protocol=pickle.HIGHEST_PROTOCOL)
        
            self.logger.info("*** Saving unfiltered dictionary...")
            dictionary.save_as_text(outputBase + 'wordids_complete.txt.bz2')
            dictionary.save(outputBase + 'wordids_complete.pkl.bz2')
            
            self.logger.info("*** Filtering dictionary...")
            dictionary.filter_extremes(no_below=freqLowerBound, no_above=freqLimit, keep_n=keepWords)
            
            self.logger.info("*** Saving dictionary...")
            dictionary.save_as_text(outputBase + 'wordids.txt.bz2')
            dictionary.save(outputBase + 'wordids.pkl.bz2')
        else:
            self.logger.info("*** Loading Page IDs...");
            pageIDs = pickle.load(smart_open(outputBase + "pageids.pkl", 'rb'))            
            self.logger.info("*** Loading dictionary...")
            dictionary = Dictionary.load(outputBase + "wordids.pkl.bz2")
        
        tfidf = None
        if tfidfPath == None:
            self.logger.info("*** Creating TF/IDF corpus...")
            tfidf = TfidfModel(wiki, id2word=dictionary, normalize=True)
            tfidfCorpus = tfidf[wiki]
            
            MmCorpus.serialize(outputBase + 'tfidf.mm', tfidfCorpus, progress_cnt=10000)
        else:
            self.logger.info("*** Loading TF/IDF corpus from %s...", tfidfPath)
            tfidfCorpus = MmCorpus(smart_open(tfidfPath, 'rb'))
    
        if wiki != None:
            del wiki
        
        if self.options.lda:
            if os.path.exists(outputBase + 'lda.pkl.bz2'):
                self.logger.info("*** Loading LDA model...")
                model = LdaModel.load(outputBase + 'lda.pkl.bz2')
            else:        
                self.logger.info("*** Creating LDA model...")
                model = LdaModel(corpus=tfidfCorpus, id2word=dictionary, num_topics=vectorLength)
                self.logger.info("*** Saving LDA model...")
                model.save(outputBase + 'lda.pkl.bz2')
        else:
            if os.path.exists(outputBase + 'lsi.pkl.bz2'):
                self.logger.info("*** Loading LSA model...")
                model = LsiModel.load(outputBase + 'lsi.pkl.bz2')
            else:        
                self.logger.info("*** Creating LSA model...")
                model = LsiModel(corpus=tfidfCorpus, id2word=dictionary, num_topics=vectorLength)
                self.logger.info("*** Saving LSA model...")
                model.save(outputBase + 'lsi.pkl.bz2')
            # sometimes, gensim just happens to change the vector length, so be sure!
            vectorLength = model.projection.u.shape[1]
        
        if tfidf != None:
            del tfidf
    
        self.logger.info("*** Saving vectors...")
        #MmCorpus.serialize(options.output + '_lsa.mm', lsiCorpus, progress_cnt=10000)
        with openLSAVectorsFile(outputBase + ('lda.dat' if self.options.lda else 'lsa.dat'), 'wb', vecLen=vectorLength) as outFile:
            for i, tup in enumerate(itertools.izip(pageIDs, tfidfCorpus)):
                p_id, doc = tup
                # generate LSA vector
                if i % 10000 == 0:
                    self.logger.info("%d documents processed" % i)
                # this is a hack; I modified gensim to return a numpy array instead of a sparse vector.
                vec = model[doc]
                if len(vec) == vectorLength:
                    outFile.writeVector(p_id, vec)
                else:
                    log_warning("encountered vector of length", len(vec), "for article ID", p_id, "index", i)
        
if __name__ == '__main__':
    sys.exit(BuildDocumentVectors().exitCode)       