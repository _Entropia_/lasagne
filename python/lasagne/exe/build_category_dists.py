from lasagne.test import TestCase
from lasagne import BASE_CATEGORIES, log_debug, log_exception, log_info,\
    log_error
from gensim.utils import smart_open
import multiprocessing
import cPickle as pickle
import os
import datetime
import traceback
import sys
from lasagne.utils.delegate_cache import DelegateCache

inst = None
                
def processBaseCategory(rootCat):
    global inst
    
    try:
        delCache = DelegateCache(inst)
        
        log_info("*** Processing %s ***" % delCache.baseCategoryNames[rootCat])
        
        cnx = inst.newConnection();
        cursor = cnx.cursor(prepared=True)
        
        distances = {}
        curDepth = 0
        curDepthQueue = [rootCat]
        nextDepthQueue = []
    
        while len(curDepthQueue) > 0:
            baseCat = curDepthQueue.pop()
            log_debug("Fetching subcats for", baseCat)
            
            subcats = [aCat for aCat in delCache.dbHandler.getSubcats(baseCat, cursor) if aCat not in distances]
            for aSubcat in subcats:
                if aSubcat in delCache.baseCatSet:
                    # no distances between base categories
                    continue
                distances[aSubcat] = curDepth + 1
            nextDepthQueue.extend((cat for cat in subcats if not cat in delCache.baseCatSet))
            
            if len(curDepthQueue) == 0:
                # current depth is finished, swap queues and increment depth
                curDepthQueue, nextDepthQueue = nextDepthQueue, curDepthQueue
                log_info("%s: depth %d finished, %d categories reachable" % (delCache.baseCategoryNames[rootCat], curDepth, len(distances)))
                curDepth += 1
            
        log_info("*** Finished %s ***" % delCache.baseCategoryNames[rootCat])
        return distances
    except:
        log_exception("! Error in %d" % rootCat)
    finally:
        cursor.close()

class TestClient(TestCase):
    def _configOptionParser(self):
        TestCase._configOptionParser(self)
    
    def _runTest(self):
        outPath = os.path.join(self.options.outputFolder, "categorydists")
        if os.path.exists(outPath + ".pkl.bz2"):
            log_error("Output file already exists.")
            return False
        
        self.initializeBaseCategories()
        self.buildDict(outPath)
    
    def _getOutputFolderName(self):
        return 'categorydists'
    
    def buildDict(self, outPath):
        global inst
        inst = self
        startTime = datetime.datetime.now()
        
        #self.baseCategories = set()
        #self.baseCategoryNames[self.getCategoryID("Bananas", sensitive=True)] = "Bananas"
        #self.baseCategories.add(self.getCategoryID("Bananas", sensitive=True))
        processes = max(1, min(len(self.baseCategories), multiprocessing.cpu_count()))
        log_info("starting %d processes" % processes)
        pool = multiprocessing.Pool(processes)
        distanceDicts = pool.map(processBaseCategory, self.baseCategories)
            
        log_info("*** Creating final dictionary ***")
        allCats = set()
        for aDict in distanceDicts:
            allCats.update(aDict.keys())
            
        allDists = {cat : [aDict[cat] if cat in aDict else -1 for aDict in distanceDicts] for cat in allCats}
        self.writeDict(allDists, outPath)
        endTime = datetime.datetime.now()
        log_info("Consumed time: %s sec" % (endTime - startTime).total_seconds())
    
    def writeDict(self, catDict, outPath):
        log_debug("Writing Pickle output to %s.pkl.bz2..." % outPath)
        with smart_open(outPath + ".pkl.bz2", 'wb') as outFile:
            pickle.dump(catDict, outFile, protocol=pickle.HIGHEST_PROTOCOL)
        
        log_debug("Writing TSV output to %s.tsv.bz2..." % outPath)
        with smart_open(outPath + ".tsv.bz2", 'wb') as outFile:
            outFile.write("Category\t")
            outFile.write('\t'.join(BASE_CATEGORIES))
            outFile.write('\n')
            for key, cats in catDict.iteritems():
                outFile.write(str(key))
                outFile.write('\t')
                outFile.write('\t'.join(str(cat) for cat in cats))
                outFile.write('\n')

if __name__ == '__main__':
    sys.exit(TestClient().exitCode)
        
