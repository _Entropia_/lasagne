import mysql.connector

class DatabaseHandler(object):
    SUBCATS_STMT = "select from_id from categorytree2 where to_id = ?"
    PARCATS_STMT = "select to_id from categorytree2 where from_id = ?"
    ART_PARCATS_STMT = "select to_id from articletree2 where from_id = ?"
    SELECT_ID_CS_BASE = """select page_id, page_namespace from page2 where convert(page_title USING utf8) collate utf8_bin LIKE ?"""
    SELECT_ID_CI_BASE = """select page_id, page_namespace from page2 where page_title LIKE ?"""
    NAMESPACE_FROM_ID_STMT = """select page_namespace, page_title from page2 where page_id = ?"""
    TITLE_FROM_ID_STMT = """select page_title from page2 where page_id = ?"""
    
    def __init__(self, host, user, password):
        self.cnx = None
        self.host = host
        self.user = user
        self.password = password
        
    def _checkConnected(self):
        if self.cnx != None:
            return
        self.cnx = self.newConnection()
    
    def close(self):
        if self.cnx != None:
            self.cnx.close()
    
    def newConnection(self):
        return mysql.connector.connect(host=self.host, user=self.user, password=self.password, database='WIKI_DB')
    
    def cursor(self, *args, **kwArgs):
        self._checkConnected()
        return self.cnx.cursor(*args, **kwArgs)
            
    def encode(self, val):
        if type(val) == unicode:
            return val.encode('utf-8')
        return val
            
    def query(self, query, queryArgs, cursor=None):
        self._checkConnected()
        closeCursor = False
        if cursor == None:
            cursor = self.cnx.cursor(prepared=True)
            closeCursor = True
        try:
            query = self.encode(query)
            queryArgs = [self.encode(val) for val in queryArgs]
            cursor.execute(query, queryArgs)
            return [row for row in cursor]
        finally:
            if closeCursor:
                cursor.close()
                
    def firstRow(self, query, queryArgs, cursor=None):
        return self.query(query, queryArgs, cursor)[0]
    
    def getPageName(self, pID, cursor=None):
        try:
            return self.firstRow(self.TITLE_FROM_ID_STMT, (pID,), cursor=cursor)[0]
        except IndexError:
            return None
    
    def getArticleID(self, pName, cursor=None, sensitive=False):
        base = self.SELECT_ID_CI_BASE if not sensitive else self.SELECT_ID_CS_BASE
        return self.firstRow(base + " and page_namespace = 0", (pName,), cursor=cursor)[0]
    
    def getCategoryID(self, pName, cursor=None, sensitive=False):
        base = self.SELECT_ID_CI_BASE if not sensitive else self.SELECT_ID_CS_BASE
        return self.firstRow(base + " and page_namespace = 14", (pName,), cursor=cursor)[0]

    def getIDs(self, pName, cursor=None, sensitive=False):
        base = self.SELECT_ID_CI_BASE if not sensitive else self.SELECT_ID_CS_BASE
        return self.query(base + " and (page_namespace = 14 or page_namespace = 0)", (pName,), cursor=cursor)

    def getNamespace(self, pID, cursor = None):
        try:
            return self.firstRow(self.NAMESPACE_FROM_ID_STMT, (pID,), cursor=cursor)[0]
        except IndexError:
            return None
        
    def getNamespaceAndTitle(self, pID, cursor = None):
        try:
            return self.firstRow(self.NAMESPACE_FROM_ID_STMT, (pID,), cursor=cursor)
        except IndexError:
            return None

    def getSubcats(self, catID, cursor=None):
        return [row[0] for row in self.query(self.SUBCATS_STMT, (catID,), cursor)]
    
    def getParentcats(self, catID, cursor=None):
        return [row[0] for row in self.query(self.PARCATS_STMT, (catID,), cursor)]

    def getArticleParentcats(self, articleID, cursor=None):
        return [row[0] for row in self.query(self.ART_PARCATS_STMT, (articleID,), cursor)]
