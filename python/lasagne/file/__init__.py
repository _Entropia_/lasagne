from gensim.utils import smart_open
from lasagne import log_debug

def countLines(f):
    if type(f) == str:
        f = smart_open(f, 'rb')
        close = True
    else:
        close = False
    
    try:
        lines = 0
        buf_size = 1024 * 1024
        read_f = f.read # loop optimization
    
        nextP = 10000
        buf = read_f(buf_size)
        while buf:
            lines += buf.count('\n')
            buf = read_f(buf_size)
            if lines > nextP:
                log_debug(nextP, "lines counted")
                nextP *= 2
    
        return lines
    finally:
        if close:
            f.close()