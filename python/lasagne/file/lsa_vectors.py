from lasagne.file import smart_open, countLines
from lasagne import log_exception, log_info, log_debug
import itertools
import numpy
import os

LSA_VECTORS_TSV = 0
LSA_VECTORS_BINARY_32 = 1
LSA_VECTORS_BINARY_64 = 2

_SUPPORTED_VERSIONS = [LSA_VECTORS_TSV, LSA_VECTORS_BINARY_32, LSA_VECTORS_BINARY_64]

def _generateFile(f, version, vecLen = -1):
    if version == LSA_VECTORS_TSV: 
        return _TSVLSAFile(f , version, vecLen)
    elif version == LSA_VECTORS_BINARY_32:
        return _BinaryLSAFile(f, version, numpy.float32, 4, vecLen)
    elif version == LSA_VECTORS_BINARY_64:
        return _BinaryLSAFile(f, version, numpy.float64, 8, vecLen)
    else:
        raise NotImplementedError("Unknown file version.")

def _determineVersion(f):
    # check if first byte is 0
    c = f.read(1)
    if c != '\0':
        f.seek(-1, 1)
        return LSA_VECTORS_TSV
    else:
        # check version
        version = ord(f.read(1))
        if not version in _SUPPORTED_VERSIONS:
            f.seek(-2, 1)
            raise NotImplementedError("Unknown file version.")
        return version
        
def openLSAVectorsFile(path, mode, version=LSA_VECTORS_BINARY_32, vecLen=-1):
    """
    Open a new LSA vectors file for reading or writing.
    The version and the vector length are only relevant when opening a file for writing.
    """
    innerFile = smart_open(path, mode)
    if 'r' in mode:
        version = _determineVersion(innerFile)
        
    f = _generateFile(innerFile, version, vecLen)
    
    if 'w' in mode:
        f.startWriting()
    # TODO what if we're appending?
    return f

class _LSAFile(object):
    def __init__(self, f, version, vecLen = -1):
        self.iFile = f
        self._vecLen = vecLen
        self._vecCount = -1
        self._version = version
        self._initialLoc = self.iFile.tell()
        
    def __getattr__(self, name):
        # Attribute lookups are delegated to the underlying file
        # and cached for non-numeric results
        # (i.e. methods are cached, closed and friends are not)
        iFile = self.__dict__['iFile']
        a = getattr(iFile, name)
        if not issubclass(type(a), type(0)):
            setattr(self, name, a)
        return a

    # The underlying __enter__ method returns the wrong object
    # (self.iFile) so override it to return the wrapper
    def __enter__(self):
        self.iFile.__enter__()
        return self
    
    def __exit__(self, *args):
        self.iFile.__exit__(*args)
    
    def startWriting(self):
        self._startWriting()
        # set initiali position after header
        self._initialLoc = self.iFile.tell()
        
    def _startWriting(self):
        pass
    
    def vectorLength(self):
        """
        Returns the length of LSA vectors in this file.
        """
        if self._vecLen == -1:
            self._vecLen = self._vectorLength()
        return self._vecLen
    
    def _vectorLength(self):
        raise NotImplementedError
    
    def size(self):
        """
        Returns the number of LSA vectors in a file.
        """
        if self._vecCount == -1:
            self._vecCount = self._count()
        return self._vecCount
        
    def __len__(self):
        return self.size()
        
    def _count(self):
        raise NotImplementedError
    
    def writeVector(self, p_id, doc):
        """
        Writes an LSA vector to a file.
        """
        return self._writeVector(p_id, doc)
            
    def _writeVector(self, p_id, doc):
        raise NotImplementedError
    
    def iterateVectors(self, resType=numpy.float32):
        self.iFile.seek(self._initialLoc)
        return self._iterate(resType)
    
    def __iter__(self):
        return self.iterateVectors()
    
    def bulkLoad(self):
        """
        Bulk loads the LSA data and returns a tuple
        (pageIDs, lsaData)
        """
        log_info("Counting input lines...")
        numLines = self.size()
        log_info("Determining vector length...")
        vecLen = self.vectorLength()
        
        pageIDs = numpy.empty(numLines, numpy.int32)
        lsaData = numpy.empty((numLines, vecLen), numpy.float32)
    
        log_info("Loading LSA vectors...")
        for i, tup in enumerate(self):
            p_id, vec = tup
            pageIDs[i] = int(p_id)
            if i % 100000 == 0:
                log_debug("%d/%d vectors loaded" % (i, numLines))
            
            if type(vec) == numpy.ndarray:
                lsaData[i] = vec
            else:
                for j, val in enumerate(vec):
                    lsaData[i][j] = val
        log_debug("%d/%d vectors loaded" % (i + 1, numLines))
        
        return pageIDs, lsaData 
    
    def _iterate(self, resType):
        raise NotImplementedError
    
    def skip(self, numVectors):
        """
        Skip a given number of LSA vectors in an open file
        """
        if numVectors == 0:
            return
        self._skip(numVectors)
    
    def _skip(self, numVectors):
        raise NotImplementedError

class _TSVLSAFile(_LSAFile):
    def __init__(self, f, version, vecLen = -1):
        super(_TSVLSAFile, self).__init__(f, version, vecLen)

    def _vectorLength(self):
        oldLoc = self.iFile.tell()
        line = self.iFile.next()
        self.iFile.seek(oldLoc)
        return len(line.split('\t')) - 1
    
    def _count(self):
        oldLoc = self.iFile.tell()
        count = countLines(self.iFile)
        self.iFile.seek(oldLoc)
        return count
    
    def _writeVector(self, p_id, doc):
        self.iFile.write(str(p_id))
        self.iFile.write('\t')
        self.iFile.write('\t'.join(str(val) for val in doc))
        self.iFile.write('\n')
    
    def _iterate(self, resType):
        for line in self.iFile:
            line = line.split('\t')
            if len(line) <= 2:
                continue
            try:
                p_id = int(line[0])
                vec = (resType(s) for s in itertools.islice(line, 1, None))
            except:
                log_exception("Error processing line: '%s'" % line)
                raise
            yield p_id, vec
    
    def _skip(self, numVectors):
        for i, _ in enumerate(self.iFile):
            if i + 1 == numVectors:
                break

class _BinaryLSAFile(_LSAFile):
    def __init__(self, f, version, dtype, nBytes, vecLen = -1):
        super(_BinaryLSAFile, self).__init__(f, version, vecLen)
        self._dtype = dtype
        self._nBytes = nBytes
        
    def _startWriting(self):
        if self._vecLen == -1:
            raise NotImplementedError("You have to specity the vector length for the binary version.")
        self.iFile.write('\0')
        numpy.array(self._version, dtype=numpy.uint8).tofile(self.iFile)
        numpy.array(self._vecLen, dtype=numpy.uint16).tofile(self.iFile)
            
    def _vectorLength(self):
        length = numpy.fromfile(self.iFile, numpy.uint16, 1)[0]
        # i will never read this again, so start after the length the next time
        self._initialLoc = self.iFile.tell()
        return length
    
    def _count(self):
        vecLen = self.vectorLength()
        return (os.fstat(self.iFile.fileno()).st_size - 4) / (vecLen * self._nBytes + 4) 
    
    def _writeVector(self, p_id, doc):
        numpy.array(p_id, dtype=numpy.uint32).tofile(self.iFile)
        for val in doc:
            numpy.array(val, dtype=self._dtype).tofile(self.iFile)

    def _iterate(self, resType):
        if resType != self._dtype:
            raise NotImplementedError("Cannot convert data types.")
        try:
            vecLen = self.vectorLength()
            while True:
                p_id = numpy.fromfile(self.iFile, numpy.int32, 1)[0]
                vec = numpy.fromfile(self.iFile, self._dtype, vecLen)
                yield p_id, vec
        except IndexError:
            # finished iteration
            pass
    
    def bulkLoad(self):
        log_info("Counting input lines...")
        numLines = self.size()
        log_info("Determining vector length...")
        vecLen = self.vectorLength()
        
        pageIDs = numpy.empty(numLines, numpy.int32)
        lsaData = numpy.empty((numLines, vecLen), numpy.float32)
    
        log_info("Loading LSA vectors...")
        try:
            for i in itertools.count():
                pageIDs[i] = numpy.fromfile(self.iFile, numpy.int32, 1)[0]
                lsaData[i] = numpy.fromfile(self.iFile, self._dtype, vecLen)
                if i % 100000 == 0:
                    log_debug("%d/%d vectors loaded" % (i, numLines))
        except IndexError:
            # finished iteration
            pass
        
        log_debug("%d/%d vectors loaded" % (i, numLines))
        
        return pageIDs, lsaData 
    
    def _skip(self, numVectors):
        self.iFile.seek(numVectors * (self.vectorLength() * self._nBytes + 4), 1)
