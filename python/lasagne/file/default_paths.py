import os

if "LASAGNE_PATH" in os.environ:
    LASAGNE_PATH = os.environ["LASAGNE_PATH"]
else:
    LASAGNE_PATH = os.path.expanduser('~')
    
def _defaultPath(relPath):
    if os.path.exists(os.path.join(LASAGNE_PATH, relPath)):
        return os.path.join(LASAGNE_PATH, relPath)
    if os.path.exists(os.path.join(LASAGNE_PATH, relPath + ".bz2")):
        return os.path.join(LASAGNE_PATH, relPath + ".bz2")
    return None
    
WIKI_XML_DUMP_FILE = _defaultPath("enwiki-20131001-pages-articles.xml")
CATEGORY_DISTS_FILE = _defaultPath("categorydists/categorydists.pkl")
ENTRYCATS_FILE = _defaultPath("entrycats/entrycats.pkl")
LSA_CORPUS_FILE = _defaultPath("lsa/analysis_lsi.pkl")
LSA_VECTORS_FILE = _defaultPath("lsa/analysis_lsa.dat")
LDA_CORPUS_FILE = _defaultPath("lsa/analysis_lda.pkl")
LDA_VECTORS_FILE = _defaultPath("lsa/analysis_lda.dat")
TERM_DICTIONARY_FILE = _defaultPath("lsa/analysis_wordids.pkl")
BRAIN_FILES = [_defaultPath("d_S_h260_r010_m080/net.pkl"),
               _defaultPath("S_h208_r010_m080_b_50000_m1m1/net.pkl")]
BRAIN_FILES = [brainFile for brainFile in BRAIN_FILES if brainFile != None]
SVM_FILES = [_defaultPath("classifier/svm.pkl")]
SVM_FILES = [svmFile for svmFile in SVM_FILES if svmFile != None]
