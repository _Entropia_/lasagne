from gensim.utils import smart_open
import cPickle as pickle
import numpy.linalg
from pybrain.structure.networks import Network

class RequiredAttributeError(Exception):
    def __ini__(self, attr):
        self.attr = attr
    
    def __str__(self):
        return repr(self.attr)

class Brain:
    '''
    Provides classification for articles via Neural Network
    '''
    
    def __init__(self, net=None):
        '''
        net should contain the path to the desired neural network
        '''
        self.setNet(net)
    
    def setNet(self, net):
        '''
        set a new net by giving path to a new pickled net
        '''
        if type(net) is str:  # assume a path to the pickled net is given
            with smart_open(net, 'rb') as inFile:
                self.net = pickle.load(inFile)
            if net != None:
                fast = True
                try:
                    from arac.pybrainbridge import _RecurrentNetwork, _FeedForwardNetwork  # @UnresolvedImport
                except:
                    fast = False
                if fast:
                    self.net = self.net.convertToFastNetwork()
        else:
            self.net = net
    
    def _squash(self, vec):
        res = []
        for v in vec:
            if v <= 0:
                res.append(0)
            if v >= 1:
                res.append(1)
        return res
    
    def classify(self, vec):
        # classify lsa vector and return cleaned up 0/1 vector
        if self.net == None:
            raise RequiredAttributeError("classifier")
        
        # normalize vector
        if type(vec) != numpy.ndarray:
            vec = numpy.array(vec)
        vec = vec / numpy.linalg.norm(vec) 
        
        out = self.net.activate(vec)
        out = out.round().astype(int).tolist()
        out = self._squash(out)
        return out
