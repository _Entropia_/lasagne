import sys
from lasagne import BASE_CATEGORIES
from lasagne.file import default_paths
from gensim.utils import smart_open
import cPickle as pickle


class RequiredAttributeError(Exception):
    def __ini__(self, attr):
        self.attr = attr
    
    def __str__(self):
        return repr(self.attr)

class Categorizer(object):
    '''
    Provides classification for articles
    '''
    
    def __init__(self, entryCats=None, categoryDists=None, mode=0, margin=0):
        '''
        entryCats, categoryDists are rather self-explanatory.
        mode specifies how to categorize articles:
            - 0: convert categoryDists, then combine based on entryCats
            - 1: combine categoryDists based on entryCats, then convert
        margin specifies how many hops are added to the minimum for classification 
        '''
        if type(entryCats) is dict:  # assume unpickled dict is given
            self.entryCats = entryCats
        elif type(entryCats) is str:  # assume a path to the pickled entryCats is given
            with smart_open(entryCats, 'rb') as inFile:
                self.entryCats = pickle.load(inFile)
        elif entryCats is None:  # use default path to unpickle entryCats
            with smart_open(default_paths.ENTRYCATS_FILE, 'rb') as inFile:
                self.entryCats = pickle.load(inFile)
        
        # same procedure with categoryDists
        if type(categoryDists) is dict:  # assume unpickled dict is given
            self.categoryDists = categoryDists
        elif type(categoryDists) is str:  # assume a path to the pickled entryCats is given
            with smart_open(categoryDists, 'rb') as inFile:
                self.categoryDists = pickle.load(inFile)
        elif categoryDists is None:  # use default path to unpickle entryCats
            with smart_open(default_paths.CATEGORY_DISTS_FILE, 'rb') as inFile:
                self.categoryDists = pickle.load(inFile)
                
        self.mode = mode
        self.margin = margin
    
    def _convertDists(self, dists):
        # convert dists and return 0/1 vector
        m = min(dists, key=lambda x: x if x != -1 else sys.maxint-self.margin) + self.margin
        v = map(lambda v: 1 if v <= m and v > 0 else 0, dists)
        return v
        
    def _combineVectors(self, vecs):
        if vecs != []:
            z = zip(*vecs)
            return [max(el) for el in z]  # OR conjunction of 0/1 vectors
        else:
            # this should never happen
            return [0] * len(BASE_CATEGORIES)
            
    def _combineDistances(self, dists):
        # combine distance vectors and return resulting distance vector
        if dists != []:
            z = zip(*dists)
            return [min(el, key=lambda x: x if x != -1 else sys.maxint) for el in z]
        else:
            # this should never happen
            return [-1] * len(BASE_CATEGORIES)
    
    def categorize(self, articles):
        # categorize all articles based on settings and return a dict
        if self.entryCats == None:
            raise RequiredAttributeError("entryCats")
        if self.categoryDists == None:
            raise RequiredAttributeError("categoryDists")
        
        single = False
        if not (type(articles) is set or type(articles) is list):
            single = True
            articles = [articles]
        
        # collect all required distances
        inter = {}
        for art in articles:
            for cat in self.entryCats[art]:
                if cat in self.categoryDists and cat not in inter:
                    inter[cat] = self.categoryDists[cat]
        c = {}
        if self.mode == 0:
            # convert dists in inter
            for k in inter.keys():
                inter[k] = self._convertDists(inter[k])
            # combine 0/1 vectors as needed
            for k in articles:
                c[k] = self._combineVectors([inter[i] for i in self.entryCats[k] if i in inter])
        if self.mode == 1:
            for k in articles:
                # combine distances for required articles. Since article and category IDs are disjoint, this is perfectly safe
                inter[k] = self._combineDistances([inter[i] for i in self.entryCats[k] if i in inter])
            # even though this could be done in the same loop or even in one line, this way is faster
            for k in articles:
                # convert previously combined distances
                c[k] = self._convertDists(inter[k])
        if not single:
            return c
        else:
            return c.values()[0]
            # if c - for some reason - contains more than 1 entry at this point
            # commence eating shoes
