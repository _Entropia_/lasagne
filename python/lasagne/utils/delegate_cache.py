class DelegateCache(object):
    def __init__(self, delegate = None):
        self.delegate = delegate
        self._dbHandler = None
        self._categoryDists = None
        self._entryCats = None
        self._baseCategories = None
        self._baseCategoryNames = None
        self._baseCatSet = None
    
    @property
    def dbHandler(self):
        if self._dbHandler == None:
            self._dbHandler = self.delegate.dbHandler
        return self._dbHandler
    
    @property
    def categoryDists(self):
        if self._categoryDists == None:
            self._categoryDists = self.delegate.categoryDists
        return self._categoryDists
    
    @property
    def entryCats(self):
        if self._entryCats == None:
            self._entryCats = self.delegate.entryCats
        return self._entryCats
    
    @property
    def baseCategories(self):
        if self._baseCategories == None:
            self._baseCategories = self.delegate.baseCategories
        return self._baseCategories
    
    @property
    def baseCategoryNames(self):
        if self._baseCategoryNames == None:
            self._baseCategoryNames = self.delegate.baseCategoryNames
        return self._baseCategoryNames
    
    @property
    def baseCatSet(self):
        if self._baseCatSet == None:
            self._baseCatSet = self.delegate.baseCatSet
        return self._baseCatSet
    