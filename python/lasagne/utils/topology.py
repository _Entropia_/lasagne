from lasagne import BASE_CATEGORIES, log_exception, log_warning, log_debug, \
    log_info
import itertools
import contextlib
from lasagne.utils.delegate_cache import DelegateCache
from lasagne.classify.categorizer import Categorizer
from threading import Lock
import sys

class Topology(DelegateCache):
    ACCEPT_ARTICLES = 0x1
    ACCEPT_CATEGORIES = 0x2
        
    PAGE_ID_INDEX = 0
    PAGE_NS_INDEX = 1
    PAGE_TITLE_INDEX = 2
    CATEGORIES_INDEX = 3
    
    class CatPath(object):
        def __init__(self, cat, prev):
            self.cat = cat
            self.prev = prev
            
    def __init__(self, delegate, brains = [], categorizesModes = [(0, 0), (1, 1)]):
        super(Topology, self).__init__(delegate)
        self.categorizerModes = categorizesModes
        self._classifiers = None
        self.classifiers = brains
        self.mutex = Lock()
        self.lsi = None
    
    @property
    def categorizers(self):
        if self._classifiers == None:
            with self.mutex:
                # for thread safety, check if another thread has loaded classifier
                if self._classifiers == None:
                    self._classifiers = [Categorizer(self.entryCats, self.categoryDists, mode, margin) for mode, margin in self.categorizerModes]
        return self._classifiers
    
    def getIDs(self, aTitle, cursor = None):
        aTitle = aTitle.replace(' ', '_')
        log_debug("fetching article IDs for '%s'..." % aTitle)
        ids = [(pID, pNs) for pID, pNs in self.dbHandler.getIDs(aTitle, cursor=cursor) if pID in self.entryCats or pID in self.categoryDists]
        return ids
    
    def _articleDescription(self, _p_title, p_id, p_ns, cursor, printType=False):
        p_id = int(p_id)
        realTitle = self.dbHandler.getPageName(p_id, cursor)
        if printType:
            if p_ns == None:
                p_ns = self.dbHandler.getNamespace(p_id, cursor)
            
            if p_ns == 0:
                pType = "article"
            elif p_ns == 14:
                pType = "cat"
            else:
                pType = "unknown"
            return "%s (%s)" % (realTitle, pType)
        else:
            return "%s" % (realTitle)
    
    def _shouldFollow(self, parcat, baseCatPath, distances, pathIndices, finishedPaths):
        if not parcat in self.categoryDists:
            return False
        curDistances = self.categoryDists[parcat]
        
        follow = False
        for idx in pathIndices - set(finishedPaths.keys()):
            if curDistances[idx] == 1:
                # finished minimum path to this category
                baseCatPath = self.CatPath(self.baseCategories[idx], self.CatPath(parcat, baseCatPath))
                if idx in finishedPaths:
                    finishedPaths[idx].append(baseCatPath)
                else:
                    finishedPaths[idx] = [baseCatPath]
            elif not follow and curDistances[idx] <= distances[idx]:
                follow = True
        return follow
        
    def _minDist(self, dists):
        realDists = [dist for dist in dists if dist >= 0]
        return min(realDists) if realDists else sys.maxint
    
    def getDistances(self, pID):
        """
        Fetches the minimum base category distances for a page
        """
        if pID in self.categoryDists:
            return self.categoryDists[pID]
        elif pID in self.entryCats:
            distanceVectors = [self.categoryDists[entryCat] for entryCat in self.entryCats[pID] if entryCat in self.categoryDists]
            # check if we already are at a base category
            add_vector = [-1] * len(BASE_CATEGORIES)
            for entryCat in self.entryCats[pID]:
                for i, baseCat in enumerate(self.baseCategories):
                    if entryCat == baseCat:
                        add_vector[i] = 0
            distanceVectors.append(add_vector)
            return map(self._minDist, itertools.izip(*distanceVectors))
    
    def _IDListFromID(self, pID, cursor):
        ns = self.dbHandler.getNamespace(pID, cursor)
        if ns != None:
            return [(pID, ns)]
        else:
            return []
    
    def getEntrycats(self, searchedArticles, pageIDs=False):
        """
        Computes the entry categories for a list of articles.
        searchedArticles -- A list of article names or IDs (int)
        pageIDs -- Instead of just returning rows of readable strings,
                   return rows of (pageID, string).
                   Also, return a tuple (output, column IDs)
        
        Returns a list of output rows, the first one containing tuples
        (page ID, page title)
        Returns an empty list if no article could be processed.
        """
        try:
            log_debug("Topology getEntrycats start")
            result = None
            colIDs = []
            with contextlib.closing(self.dbHandler.newConnection()) as cnx:
                with contextlib.closing(cnx.cursor(prepared=True)) as cursor:
                    titles = []
                    entrycats = []
                    for aTitle in searchedArticles:
                        if type(aTitle) == int:
                            ids = self._IDListFromID(aTitle, cursor)
                        else:
                            ids = [aTup for aTup in self.getIDs(aTitle, cursor) if aTup[1] == 0]
                        if not ids:
                            log_info("No article found with the title '%s'." % aTitle)
                            continue
                        for anId, _aNs in ids:
                            if not anId in self.entryCats:
                                log_debug("Article ID %s id not in entry category dictionary." % anId)
                                continue
                            titles.append(self._articleDescription(aTitle, anId, 0, cursor))
                            if pageIDs:
                                colIDs.append((anId, 0, self.dbHandler.getPageName(anId, cursor)))
                            entrycats.append(self.entryCats[anId])
                
                    if titles:
                        rows = []
                        rows.append(titles)
                        
                        for i in itertools.count():
                            row = []
                            anotherRow = False
                            for entryCatList in entrycats:
                                if i < len(entryCatList):
                                    description = self._articleDescription(None, entryCatList[i], None, cursor)
                                    if pageIDs:
                                        row.append((int(entryCatList[i]), description))
                                    else:
                                        row.append(description)
                                    anotherRow = i + 1 < len(entryCatList)
                                else:
                                    row.append('')
                            rows.append(row)
                            if not anotherRow:
                                break
                        result = rows
        except:
            log_exception()
            
        if result == None:
            result = []
        
        if pageIDs:
            return result, colIDs
        else:
            return result
    
    
    def getAllDistances(self, searchedArticles):
        """
        Returns a list of (Page ID, Namespace, title, distances)
        """
        results = []
        try:
            with contextlib.closing(self.dbHandler.newConnection()) as cnx:
                with contextlib.closing(cnx.cursor(prepared=True)) as cursor:
                    for aTitle in searchedArticles:
                        if type(aTitle) == int:
                            ids = self._IDListFromID(aTitle, cursor)
                        else:
                            ids = self.getIDs(aTitle, cursor)
                            
                    for pID, pNs in ids:
                        results.append((pID,
                                        pNs,
                                        self.dbHandler.getPageName(pID, cursor),
                                        [int(val) for val in self.getDistances(pID)]))
        except:
            log_exception()
        return results
    
    def getCandidates(self, page, namespaceSelection):
        """ 
        This method searches for possible candidates
        and returns a list of tuples (pageID, pageNamespace).
        page -- The title or ID (int) of the page
        namespaceSelection -- The accepted namespaces - 
                              ACCEPT_ARTICLES, ACCEPT_CATEGORIES or both
        
        Returns a list of tuples (pageID, pageNamespace).
        Returns an empty list if no article was found.
        """
        try:
            accepted = set()
            if namespaceSelection & self.ACCEPT_ARTICLES:
                accepted.add(0)
            if namespaceSelection & self.ACCEPT_CATEGORIES:
                accepted.add(14)
            with contextlib.closing(self.dbHandler.newConnection()) as cnx:
                with contextlib.closing(cnx.cursor(prepared=True)) as cursor:
                    if type(page) == int:
                        ids = [(pID, ns) for pID, ns in self._IDListFromID(page, cursor) if ns in accepted]
                    else:
                        ids = [row for row in self.getIDs(page, cursor) if row[0] in self.categoryDists or row[0] in self.entryCats]
                    if len(ids) == 0:
                        log_warning("No article found with title/ID '%s'." % page)
                    
                    return ids
        except:
            log_exception()
        return []
    
    def getCategoryPaths(self, pageID, pageNamespace, baseCats=None, pageIDs=False):
        """ 
        Computes the minimum path from an article to the base categories.
        pageID -- The page ID of an article or a category
        pageNamespace -- The namespace of the page
        baseCats -- The base categories (title or ID) to compute the paths
        pageIDs -- Instead of just returning result of readable strings,
                   return result of (pageID, string)
                   Also, return a tuple (output, column IDs)
        
        Returns a list of output result, the first one containing the
        base category names.
        Returns an empty list if no article was found.
        """
        try:
            result = None
            colIDs = None
            with contextlib.closing(self.dbHandler.newConnection()) as cnx:
                with contextlib.closing(cnx.cursor(prepared=True)) as cursor:
                    if not baseCats:
                        pathIndices = set(range(16))
                    else:
                        pathIndices = set()
                        baseCatDict = {baseCat.strip().lower(): idx for idx, baseCat in enumerate(BASE_CATEGORIES)}
                        for aCat in baseCats:
                            if type(aCat) == int:
                                if not aCat in self.baseCategories:
                                    log_warning(aCat, " is no base category.")
                                    continue
                                pathIndices.add(self.baseCategories.index(aCat))
                            else:
                                dictRep = aCat.strip().lower()
                                if not dictRep in baseCatDict:
                                    log_warning("There is no base category '%s'." % aCat)
                                    continue
                                pathIndices.add(baseCatDict[dictRep])
                            
                    colIDs = [(self.baseCategories[idx], 14, self.baseCategoryNames[self.baseCategories[idx]]) for idx in pathIndices] 
                    finishedPaths = {}
                    # make copy
                    distances = [val for val in self.getDistances(pageID)]
                    
                    curDepthQueue = []
                    start = self.CatPath(pageID, None)
                    if pageNamespace == 0:
                        # check if we already are at a base category
                        for entryCat in self.entryCats[pageID]:
                            for idx in pathIndices:
                                if self.baseCategories[idx] == entryCat:
                                    finishedPaths[idx] = [self.CatPath(int(entryCat), start)]
                                    # there can be no more paths in this case
                                    break
                        curDepthQueue.extend(self.CatPath(int(entryCat), start) for entryCat in self.entryCats[pageID] if self._shouldFollow(entryCat, None, distances, pathIndices, finishedPaths))
                    else:
                        # check if there there is a direct super-category that is a base category
                        for idx in pathIndices:
                            if distances[idx] == 1:
                                finishedPaths[idx] = [self.CatPath(int(self.baseCategories[idx]), start)]
                        curDepthQueue.append(start)
                    nextDepthQueue = []
        
                    while len(curDepthQueue) > 0 and len(finishedPaths) < len(pathIndices):
                        baseCatPath = curDepthQueue.pop()
                        parcats = self.dbHandler.getParentcats(baseCatPath.cat, cursor)
                        for parcat in parcats:
                            if self._shouldFollow(parcat, baseCatPath, distances, pathIndices, finishedPaths):
                                nextDepthQueue.append(self.CatPath(parcat, baseCatPath))
                        
                        if len(curDepthQueue) == 0:
                            # current depth is finished, swap queues and increment depth
                            curDepthQueue, nextDepthQueue = nextDepthQueue, curDepthQueue
                            for i in range(len(distances)):
                                distances[i] -= 1
        
                    result = []
                    result.append([BASE_CATEGORIES[idx] for idx in sorted(pathIndices)])
                    
                    printPaths = [(0, idx, finishedPaths[idx][0] if idx in finishedPaths else None) for idx in sorted(pathIndices)]

                    for row in itertools.count():
                        pRow = [''] * len(printPaths)
                        anotherRow = False
                        for col in range(len(printPaths)):
                            pathIdx, idx, path = printPaths[col]
                            if path == None:
                                if idx in finishedPaths and pathIdx + 1 < len(finishedPaths[idx]):
                                    pathIdx += 1
                                    path = finishedPaths[idx][pathIdx]
                                    printPaths[col] = (pathIdx, idx, path)
                                    pRow[col] = "-----"
                                    anotherRow = True
                                elif pathIdx == 0 and row == 0:
                                    pRow[col] = "<No path>"
                            else:
                                pageName = self.dbHandler.getPageName(path.cat, cursor)
                                if pageIDs:
                                    pRow[col] = (path.cat, pageName)
                                else:
                                    pRow[col] = pageName
                                path = path.prev
                                printPaths[col] = (pathIdx, idx, path)
                                if path != None or pathIdx + 1 < len(finishedPaths[idx]):
                                    anotherRow = True
                        result.append(pRow)
                        if not anotherRow:
                            break
        except:
            log_exception()
        
        if result == None:
            result = []
        if pageIDs:
            return result, colIDs if colIDs != None else []
        else:
            return result 

    def getClassifiers(self):
        """
        Returns the names of the currently available classifiers
        (category path and neural)
        """
        classifiers = []
        if len(self.categorizerModes) > 1:
            for mode, margin in self.categorizerModes:
                classifiers.append("Categories (%d,%d)" % (mode, margin))
        elif len(self.categorizerModes) == 1:
            classifiers.append("Categories")
            
        for i, _brain in enumerate(self.classifiers):
            classifiers.append("Brain %d" % i)
    
        return classifiers
    
    def getPageDetails(self, pages, lsaVectors = None):
        """
        lsaVectors: array of LSA vectors corresponding to the page IDs in pages
        """
        try:
            with contextlib.closing(self.dbHandler.newConnection()) as cnx:
                with contextlib.closing(cnx.cursor(prepared=True)) as cursor:
                    pageIDs = []
                    for page in pages:
                        if type(page) == int:
                            pageIDs.append((page, self.dbHandler.getNamespace(page)))
                        else:
                            pageIDs.extend(self.getIDs(page, cursor))
                    
                    if lsaVectors == None and self.classifiers and self.lsi != None:
                        lsaVectors = []
                        for pID, _pNs in pageIDs:
                            lsaVec = None
                            try:
                                lsaVec = self.lsi.getLSAVector(pID)
                            except ValueError:
                                pass
                            lsaVectors.append(lsaVec)
                    
                    brainClassify = lsaVectors != None and self.classifiers
                    log_debug("Building results for %d pages..." % len(pageIDs))
                    
                    result = []
                    for i, aTup in enumerate(pageIDs):
                        pID, pNs = aTup
                        
                        classifications = []
                        
                        if pNs == 0:
                            for classifier in self.categorizers:
                                classifications.append(classifier.classify(pID))
                            if brainClassify:
                                for brain in self.classifiers:
                                    classifications.append(brain.classify(lsaVectors[i]))
                        
                        row = [pID,
                               pNs,
                               self.dbHandler.getPageName(pID, cursor),
                               classifications]
                        result.append(row)
                        
                    return result 
                  
        except:
            log_exception("Error getting page details.")
            return []
        
