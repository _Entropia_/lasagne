from lasagne import log_info, log_exception, log_debug, convert_string
from gensim.corpora.dictionary import Dictionary
from gensim.models.lsimodel import LsiModel
from gensim import utils
import numpy.linalg

class LSAResult:
    def __init__(self, dictFilePath, lsaFilePath):
        try:
            log_info("Loading term dictionary...")
            self.dictionary = Dictionary.load(dictFilePath)
            
            log_info("Loading LSI model...")
            self.lsi = LsiModel.load(lsaFilePath)
        except:
            log_exception("Error trying to load LSA results from '%s'" % lsaFilePath)
        
    def trimString(self, s):
        if len(s) < 50:
            return s
        return s[:47] + "..."
    
    def printVec(self, desc, vec):
        if len(vec) > 20:
            log_debug(u"%s: [%s, ...], length: %d" % (desc, u', '.join(convert_string(val) for val in vec[:20]), len(vec)))
        else:
            log_debug(u"%s: %s, length: %d" % (desc, str(vec), len(vec)))
    
    def generateQueryVector(self, query, pythonList = False):
        try:
            log_debug(u'Query: "%s"' % self.trimString(query))
            doc = utils.lemmatize(query)
            self.printVec(u"Lemmatized", doc)
            doc = self.dictionary.doc2bow(doc)
            self.printVec(u"Document vector", doc)
            vec = self.lsi[doc]
            self.printVec(u"LSA vector", vec)
            log_debug(u"Norm of LSA vector:", numpy.linalg.norm(vec))
            if pythonList:
                return [float(val) for val in vec]
            return vec
        except:
            log_exception()
            raise
        