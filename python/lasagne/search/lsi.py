import heapq
import numpy
from lasagne.file.lsa_vectors import openLSAVectorsFile
from scipy.spatial.distance import cosine, cdist
from lasagne import log_info, log_exception, log_error, log_debug
import multiprocessing
from functools import partial
import itertools
from bisect import bisect_left
from lasagne.search.lsa_result import LSAResult
import timeit

inst = None

def searchWrapper(n, queryVec, matDists, aSlice):
    global inst
    return inst._topN(n, queryVec, matDists, aSlice)

class LSI:
    def __init__(self, vecFilePath, lsaResult, topology=None):
        try:
            self.lsaResult = lsaResult
            self.topology = topology
            
            lsaFile = openLSAVectorsFile(vecFilePath, 'rb')
            self.pageIDs, self.lsaData = lsaFile.bulkLoad()
        except:
            log_exception("Error trying to load LSA vectors from '%s'" % vecFilePath)
        
    def size(self):
        return len(self.pageIDs)
    
    def __len__(self):
        return self.size()
    
    def _topN(self, n, queryVec, matDists, aSlice):
        if matDists:
            mat1 = numpy.array([queryVec], copy=False)
            # log_debug("Generating slice")
            mat2 = self.lsaData[aSlice[0]:aSlice[1]]
            
            # log_debug("Computing distances")
            distances = cdist(mat1, mat2, 'cosine')
            # log_debug("Computing smallest n")
            nsmallest = heapq.nsmallest(n,
                                        enumerate(distances[0]),
                                        key=lambda tup : tup[1])
            return [(self.pageIDs[i + aSlice[0]], dist) for i, dist in nsmallest]
        else:
            nsmallest = heapq.nsmallest(n,
                                        enumerate(itertools.islice(self.lsaData, aSlice[0], aSlice[1])),
                                        key=lambda tup : cosine(queryVec, tup[1]))
        return [(self.pageIDs[i + aSlice[0]], vec) for i, vec in nsmallest]
    
    def flattenVectors(self, lists):
        for aList in lists:
            for p_id, data in aList:
                yield p_id, data
    
    def _searchVector(self, vec, n=10, parallel=True, materializeDists=False, details=False):
        start = timeit.default_timer()
        if parallel:
            global inst
            inst = self
            
            processes = max(1, multiprocessing.cpu_count())
            log_info("starting %d processes" % processes)
            
            pool = multiprocessing.Pool(processes)
            
            sliceLen = len(self.lsaData) / processes
            lastSliceEnd = processes * sliceLen + len(self.lsaData) % processes
            slices = [(i * sliceLen, lastSliceEnd if i == processes - 1 else (i + 1) * sliceLen) for i in range(processes)]
                
            function = partial(searchWrapper, n, vec, materializeDists)
            if materializeDists:
                nsmallest = heapq.nsmallest(n,
                                            self.flattenVectors(pool.imap(function, slices)),
                                            key=lambda tup : tup[1])
            else:
                nsmallest = heapq.nsmallest(n,
                                            self.flattenVectors(pool.imap(function, slices)),
                                            key=lambda tup : cosine(vec, tup[1]))
            pool.terminate()
        else:
            nsmallest = self._topN(n, vec, (0, len(self.lsaData)))
        
        stop = timeit.default_timer()
        log_debug("Query runtime: %ss" % (stop - start))
        
        pageIDs = [int(pageID) for pageID, _ in nsmallest]
        if details:
            if not materializeDists:
                lsaVectors = [vec for _, vec in nsmallest]
            else:
                lsaVectors = [self.getLSAVector(pID) for pID, _ in nsmallest]
            return self.topology.getPageDetails(pageIDs, lsaVectors)
        
        return pageIDs 
    
    def _getVectorIndex(self, pID):
        i = bisect_left(self.pageIDs, pID)
        if i != len(self.pageIDs) and self.pageIDs[i] == pID:
            return i
        raise ValueError("There is no article with the ID", pID)
    
    def getLSAVector(self, pID):
        return self.lsaData[self._getVectorIndex(pID)]
    
    def searchSimilarArticles(self, articleID, n=10, parallel=True, materializeDists=False, details=False):
        if details and self.topology == None:
            log_error("Topology is None, cannot fetch details.")
            return []
        
        vec = self.getLSAVector(articleID)
        return self._searchVector(vec, n, parallel, materializeDists, details)
    
    def search(self, query, n=10, parallel=True, materializeDists=False, details=False):
        if details and self.topology == None:
            log_error("Topology is None, cannot fetch details.")
            return []
        
        return self._searchVector(self.lsaResult.generateQueryVector(query), n, parallel, materializeDists, details)
    
if __name__ == '__main__':
    from lasagne.file import default_paths
    from lasagne import setLogFile
    import os 
    setLogFile(os.path.expanduser("~/performance.log"))
    lsaResult = LSAResult(default_paths.TERM_DICTIONARY_FILE, default_paths.LSA_CORPUS_FILE)
    index = LSI(default_paths.LSA_VECTORS_FILE, lsaResult)
    res = index.search("test")
    log_info(res)
    
