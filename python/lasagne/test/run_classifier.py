from lasagne import BASE_CATEGORIES, log_info, log_error, log_debug
from lasagne.test import TestCase
from lasagne.file import lsa_vectors
from lasagne.classify.categorizer import Categorizer
from gensim.utils import smart_open
import cPickle as pickle
import os
import datetime
import numpy, numpy.random

class TrainClassifier(TestCase):
    def _configOptionParser(self, needMySQL=False):
        TestCase._configOptionParser(self, needMySQL=needMySQL)
        self.optionParser.add_option("--classifier",
                  action="store", dest="classifier", default=None,
                  help="Read pickled classifier from FILE.", metavar="FILE")
        self.optionParser.add_option("-l", "--vectors",
                  action="store", type="string", dest="vectors", default=None,
                  help="Read document vectors from FILE.", metavar="FILE")
        
        self.optionParser.add_option("--train",
                  action="store_true", dest="train", default=False,
                  help="Train network")
        self.optionParser.add_option("--validate",
                  action="store_true", dest="validate", default=False,
                  help="Validate network")
        self.optionParser.add_option("--demo",
                  action="store_true", dest="demo", default=False,
                  help="Test classifier with a small demo data set.")

        self.optionParser.add_option("-t",
                  action="store", dest="trainingSet", default=None,
                  help="Read pickled Training Set from FILE", metavar="FILE")
        
        # Categorizer options
        self.optionParser.add_option("--mode",
                  action="store", type="int", dest="mode", default=0,
                  help="Set mode. 0: convert, then combine | 1: combine, then convert")
        self.optionParser.add_option("--margin",
                  action="store", type="int", dest="margin", default=0,
                  help="Set margin which is added to the minimum distance for classification")

    def _validate(self, docs, classifier):
        errors = []
        articleErrors = 0
        resSums = [0] * len(BASE_CATEGORIES)  # sum of assignments per category (by NN)
        tarSums = [0] * len(BASE_CATEGORIES)  # sum of assignments per category (target)
        for i, aTuple in enumerate(docs):
            vec, tar = aTuple
            res = classifier.classify(vec)
            if self.options.demo:
                log_debug(vec, tar, res)
            resSums = map(sum, zip(res, resSums))
            tarSums = map(sum, zip(tar, tarSums))
            diff = [r - t for r, t in zip(res, tar) if r != t]
            if len(diff) > 0:  # an error was made
                errors.extend(diff)
                articleErrors += 1
        log_info("%d (%g %%) errors, %d (%g %%) wrongly classified articles" % (len(errors), round(float(len(errors)) / (len(BASE_CATEGORIES) * i) * 100.0, 2),
                                                                                        articleErrors, round(float(articleErrors) / i * 100.0, 2)))
        log_info("Error tendency: %g" % numpy.mean(errors))
        log_info("Sum of assignments by net:")
        log_info("%s" % str(resSums))
        log_info("Sum of target assignments:")
        log_info("%s" % str(tarSums))

    def _classifierName(self):
        """Specifies the output file name"""
        return "classifier"

    def _getDemoData(self):
        numDocs = 100
        rand = numpy.random.random(4 * numDocs)
        
        data = []
        for i in xrange(numDocs):
            vec = rand[4 * i:4 * i + 4]
            classes = [int(round((vec[0] + 0.5 * (vec[1] + vec[2])) / 2)),
                       int(round((vec[3] + 0.5 * (vec[1] + vec[2])) / 2))]
            data.append((vec, classes))
            log_debug(vec, classes)
        return data

    def _runTest(self):        
        outPath = os.path.join(self.options.outputFolder, "%s.pkl" % self._classifierName())
        
        startTime = datetime.datetime.now()
        
        if not self.options.demo:
            if self.options.trainingSet == None:
                log_error("Training Set must be specified in any case!")
                return False
            
            # Unpickle Training Set from File
            log_info("Loading Training Set from %s" % self.options.trainingSet)
            with smart_open(self.options.trainingSet, 'rb') as inFile:
                ts = pickle.load(inFile)
            log_info("Training Set loaded with %d IDs" % len(ts))
            
            categorizer = Categorizer(entryCats=self.entryCats,
                                      categoryDists=self.categoryDists,
                                      mode=self.options.mode,
                                      margin=self.options.margin)
            
            master = set(self.entryCats.keys())
            
            if self.options.train:
                log_info("Classifying Training Set")
                categorized = categorizer.categorize(ts)
            if self.options.validate:
                log_info("Classifying all documents")
                categorized = categorizer.categorize(master)
                
            log_info("%d articles classified" % len(categorized))
            
            vectorsPath = self.options.vectors
            if not vectorsPath:
                vectorsPath = self.documentVectorsPath()
            log_info("Loading document vectors from %s" % vectorsPath)
            vectors = lsa_vectors.openLSAVectorsFile(vectorsPath, 'rb')
            vecLen = int(vectors.vectorLength())
            numCats = len(BASE_CATEGORIES)
        else:
            # demo training set
            demoData = self._getDemoData()
            vectors = [(i, aTuple[0]) for i, aTuple in enumerate(demoData)]
            categorized = {i : aTuple[1] for i, aTuple in enumerate(demoData)}
            vecLen = len(demoData[0][0])
            numCats = len(demoData[0][1])
            
            master = set(range(len(categorized)))
            ts = set(range(len(categorized) / 2))
        
        log_info("Scanning LSA Vectors and building DataSet")
        
        delegate = self._getClassificationDelegate()  # TODO implement
        
        if self.options.validate:
            other = master.difference(ts)
        if self.options.validate or self.options.train:
            trainingSet = delegate.createTrainingSet(len(ts), vecLen, numCats)
            for article, vec in vectors:
                if article in ts:
                    trainingSet.addSample(vec, categorized[article])
            log_info("DataSet (Training) complete with %d samples" % len(trainingSet))
        
        if(self.options.classifier != None):
            log_info("Loading classifier from %s" % self.options.classifier)
            classifier = delegate.loadClassifier(self.options.classifier)
        elif self.options.validate and not self.options.train and os.path.exists(outPath):
            # load from default path
            classifier = delegate.loadClassifier(outPath)
        else:
            classifier = delegate.createClassifier(vecLen, numCats, outPath)
        
        if self.options.train:
            log_info("Training classifier...")
            delegate.train(classifier, trainingSet)
            # Pickle and write net back to file
            log_info("Saving classifier to %s..." % outPath)
            delegate.saveClassifier(classifier, outPath)

        if self.options.validate:
            log_info("Validating Training Set")
            self._validate(trainingSet, classifier)
            
            log_info("Validating Unknown Set")
            self._validate(((vec, categorized[aID]) for aID, vec in vectors if aID in other), classifier)
            
        endTime = datetime.datetime.now()
        log_info("Consumed time: %s sec" % (endTime - startTime).total_seconds())
        
    def _getOutputFolderName(self):
        return 'classifier'
