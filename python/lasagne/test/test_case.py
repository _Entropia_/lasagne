from optparse import OptionParser
import os, logging, getpass
from lasagne import BASE_CATEGORIES, setLogFile, log_info, log_debug,\
    log_exception, setLoggingLevel
from lasagne.file import default_paths
from lasagne.database import DatabaseHandler
import contextlib 
from lasagne.file import smart_open
import cPickle as pickle
from lasagne.utils.logging_mutex import loggingMutex
import numpy
import itertools

class TestCase(object):
    BASECAT_STMT = "SELECT page_id FROM page WHERE page_title = ? AND page_namespace = 14"
    
    MAX_COL_WIDTH = 60
    MAX_TOTAL_WIDTH = 100
    COL_DEL = "  "
    SEPARATOR = "="
    
    def __init__(self):
        self._dbHandler = None
        
        self.options = None
        self.args = None
        self._categoryDists = None
        self._entryCats = None
        
        self.dbHandlerMutex = loggingMutex("dbHandler")
        self.entryCatsMutex = loggingMutex("entryCats")
        self.categoryDistsMutex = loggingMutex("categoryDists")
        self.baseCategoriesMutex = loggingMutex("baseCategories")
        
        self.outputTable = []
        self._configOptionParser()
        res = self._run()
        
        self.exitCode = 0
        if res == False:
            self.exitCode = -1
        elif type(res) == int:
            self.exitCode = res
        

    def _initializeLogger(self):
        log_file = os.path.join(self.options.outputFolder, "%s.log" % self._getOutputFolderName())
        setLogFile(log_file)
        self.logger = logging.getLogger()

    def _configOptionParser(self, needMySQL=True):
        usage = "usage: %prog [options] [inFile]"
        self.optionParser = OptionParser(usage=usage)
        if needMySQL:
            self.optionParser.add_option("--no-password",
                              default=False, dest="noPassword", action="store_true",
                              help="Don't ask for a MySQL password.")
            self.optionParser.add_option("-u", "--user",
                              action="store", dest="user", default="root",
                              help="Set the MySQL database user.")
            self.optionParser.add_option("--host",
                              action="store", dest="host", default="127.0.0.1",
                              help="Set the MySQL host name.")
            self.optionParser.add_option("--password",
                              action="store", dest="password", default=None,
                              help="Set the MySQL password.")
        self.optionParser.add_option('-c', "--category-dists",
                          dest="categoryDict", action="store",
                          default=default_paths.CATEGORY_DISTS_FILE,
                          help="Specify the category dictionary file.")
        self.optionParser.add_option('-e', "--entrycats",
                          dest="entryCatDict", action="store",
                          default=default_paths.ENTRYCATS_FILE,
                          help="Specify the page entry category dictionary file.")
        self.optionParser.add_option("-o", "--output-folder",
                          action="store", dest="outputFolder",
                          default=os.path.join(default_paths.LASAGNE_PATH, self._getOutputFolderName()),
                          help="Set the output folder for files and logs.")
        self.optionParser.add_option("-f", "--fast-loading",
                          action="store_true",default=False,dest="fastLoading",help="Enable fast loading of dictionaries if possible.")
        self.optionParser.add_option("--lda",
                          action="store_true",default=False,dest="lda",help="Use LDA instead of LSA.")
        self.optionParser.add_option("-v", "--verbose",
                          action="store_true",default=False,dest="verbose",help="Verbose output.")

    def _getOutputFolderName(self):
        return 'test'

    @property
    def dbHandler(self):
        with self.dbHandlerMutex:
            if self._dbHandler == None:
                if self.options.noPassword:
                    mysqlPass = None
                else:
                    mysqlPass = self.options.password
                    if not mysqlPass:
                        mysqlPass = getpass.getpass("Enter MySQL password for user %s: " % self.options.user)
                self._dbHandler = DatabaseHandler(self.options.host, self.options.user, mysqlPass)
        return self._dbHandler

    def newConnection(self):
        return self.dbHandler.newConnection()

    def initializeBaseCategories(self):
        with self.baseCategoriesMutex:
            if not hasattr(self, "_baseCategories"):
                self._baseCategories = []
                self._baseCatSet = None
                self._baseCategoryNames = {}
                
                dbHandler = self.dbHandler
                with contextlib.closing(dbHandler.cursor(prepared=True)) as cursor:
                    for aCat in BASE_CATEGORIES:
                        catID = dbHandler.firstRow(self.BASECAT_STMT, (aCat,), cursor)[0]
                        self._baseCategories.append(catID)
                        self._baseCategoryNames[catID] = aCat
                    self._baseCatSet = set(self._baseCategories)
            
    def documentVectorsPath(self):
        if self.options.lda:
            return default_paths.LDA_VECTORS_FILE
        else:
            return default_paths.LSA_VECTORS_FILE
            
    @property
    def baseCategories(self):
        # initializeBaseCategories is locked
        self.initializeBaseCategories()
        return self._baseCategories
    
    @property
    def baseCatSet(self):
        # initializeBaseCategories is locked
        self.initializeBaseCategories()
        return self._baseCatSet
    
    @property
    def baseCategoryNames(self):
        # initializeBaseCategories is locked
        self.initializeBaseCategories()
        return self._baseCategoryNames
    
    @property
    def categoryDists(self):
        with self.categoryDistsMutex:
            if self._categoryDists == None:
                fastDataPath = os.path.join(os.path.dirname(self.options.categoryDict), "categorydists_data.npy")
                if self.fastLoading and os.path.exists(fastDataPath):
                    log_info("Loading category dictionary from %s" % fastDataPath)
                    try:
                        data = numpy.load(fastDataPath)
                        pIDs = numpy.load(os.path.join(os.path.dirname(self.options.categoryDict), "categorydists_pIDs.npy"))
                        
                        self._categoryDists = {}
                        for pID, dists in itertools.izip(pIDs, data):
                            self._categoryDists[int(pID)] = dists

                        del data
                        del pIDs
                    except:
                        log_exception("Fast loading failed.")
                        self._categoryDists = None
                if not self._categoryDists:
                    log_info("Loading category dictionary from %s" % self.options.categoryDict)
                    self._categoryDists = pickle.load(smart_open(self.options.categoryDict))
        return self._categoryDists
    
    @property
    def entryCats(self):
        with self.entryCatsMutex:
            if self._entryCats == None:
                fastDataPath = os.path.join(os.path.dirname(self.options.entryCatDict), "entrycats_data.npy")
                if self.fastLoading and os.path.exists(fastDataPath):
                    log_info("Loading entry category dictionary from %s" % fastDataPath)
                    try:
                        data = numpy.load(fastDataPath)
                        pIDs = numpy.load(os.path.join(os.path.dirname(self.options.entryCatDict), "entrycats_pIDs.npy"))
                        lengths = numpy.load(os.path.join(os.path.dirname(self.options.entryCatDict), "entrycats_lengths.npy"))
                        
                        self._entryCats = {}
                        offset = 0
                        for pID, length in itertools.izip(pIDs, lengths):
                            self._entryCats[int(pID)] = data[offset:offset + length]
                            offset += length

                        del data
                        del pIDs
                        del lengths                            
                    except:
                        log_exception("Fast loading failed.")
                        self._entryCats = None
                if not self._entryCats:
                    # could not fast load
                    log_info("Loading entry category dictionary from %s" % self.options.entryCatDict)
                    self._entryCats = pickle.load(smart_open(self.options.entryCatDict, 'rb'))
        return self._entryCats    
    
    def getPageName(self, pID, cursor=None):
        return self.dbHandler.getPageName(pID, cursor)
    
    def getArticleID(self, pName, cursor=None, sensitive=False):
        return self.dbHandler.getArticleID(pName, cursor, sensitive)
    
    def getCategoryID(self, pName, cursor=None, sensitive=False):
        return self.dbHandler.getCategoryID(pName, cursor, sensitive)

    def getIDs(self, pName, cursor=None, sensitive=False):
        return self.dbHandler.getIDs(pName, cursor, sensitive)

    def getSubcats(self, catID, cursor=None):
        return self.dbHandler.getSubcats(catID, cursor)
    
    def getParentcats(self, catID, cursor=None):
        return self.dbHandler.getParentcats(catID, cursor)

    def getArticleParentcats(self, articleID, cursor=None):
        return self.dbHandler.getArticleParentcats(articleID, cursor)

    def _convertToString(self, value):
        if type(value) in (str, unicode):
            return value
        if type(value) == list:
            if len(value) == 1:
                return value[0]
            else:
                return ", ".join(str(v) for v in value)
        return str(value)

    def appendOutput(self, *row):
        self.outputTable.append([self._convertToString(value) for value in row])
        
    def appendSeparator(self, sepChar=None):
        if sepChar == None:
            sepChar = self.SEPARATOR
        self.outputTable.append(str(sepChar))
        
    def _cutString(self, string, maxLen):
        if maxLen >= len(string):
            return string
        
        # if string[maxLen + 1] is whitespace, it is OK
        index = string[:maxLen + 1].rfind(" ")
        if index == -1:
            return string[:maxLen]
        # include whitespace
        return string[:index + 1] 
        
    def flushOutput(self, columnDelimiter=None, maxColumnWidth=None, maxTotalWidth=None):
        if columnDelimiter == None:
            columnDelimiter = self.COL_DEL
        if maxColumnWidth == None:
            maxColumnWidth = self.MAX_COL_WIDTH
        if maxTotalWidth == None:
            maxTotalWidth = self.MAX_TOTAL_WIDTH
        columns = []
        for aRow in self.outputTable:
            if type(aRow) == str:
                # separator
                continue
            if len(aRow) > len(columns):
                for _ in range(len(aRow) - len(columns)):
                    columns.append(0)
            for col, aValue in enumerate(aRow):
                columns[col] = min(maxColumnWidth, max((columns[col], len(aValue))))
        
        totalDelWidth = (len(columns) - 1) * len(columnDelimiter)
        totalWidth = sum(columns) + totalDelWidth
        if totalWidth > maxTotalWidth:
            # TODO implement good resizing algorithm
            pass
#             ratio = float(maxTotalWidth - totalDelWidth) / totalWidth
#             newTotal = 0
#             for i in range(len(columns) - 1):
#                 columns[i] = int(round(columns[i] * ratio))
#                 newTotal += columns[i]
#             # assign remaining width to last column
#             columns[-1] = maxTotalWidth - totalDelWidth - newTotal
#             totalWidth = maxTotalWidth
        
        for aRow in self.outputTable:
            if type(aRow) == str:
                # separator
                print aRow * (totalWidth / len(aRow))
                continue
            remaining = [len(aVal) for aVal in aRow]
            totalRemaining = sum(remaining)
            
            while totalRemaining > 0:
                # print new row until nothing remains
                rowWords = []
                for col, word in enumerate(aRow):
                    cutString = self._cutString(word[len(word) - remaining[col]:], columns[col])
                    remaining[col] -= len(cutString)
                    totalRemaining -= len(cutString)
                    if cutString.endswith(" "):
                        cutString = cutString[:-1]
                    if col == len(columns):
                        # last string does not need to be padded
                        rowWords.append(cutString)
                    else:
                        rowWords.append(cutString.ljust(columns[col]))
                print columnDelimiter.join(rowWords)
        
        self.outputTable = []

    def _runTest(self):
        raise NotImplementedError("To be implemented in sub-class.")
    
    def _run(self):
        (self.options, self.args) = self.optionParser.parse_args()
        try:
            if not self.options.verbose:
                setLoggingLevel(logging.INFO)
            self.fastLoading = self.options.fastLoading
            if not os.path.exists(self.options.outputFolder):
                os.makedirs(self.options.outputFolder)
            self._initializeLogger()
            try:
                return self._runTest()
            except:
                log_exception("Error running test")
                return -1
        finally:
            if self._dbHandler != None:
                self._dbHandler.close()
