from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal
import Pyro4
from Pyro4.errors import NamingError
from lasagne.network.lasagne_server import LasagneServer
from lasagne import log_info, log_debug, log_exception,\
    log_error
from lasagne.network.ssh_tunnel import SSHTunnelHandler
from lasagne.utils.logging_mutex import loggingMutex
import itertools
        
class LasagneClient(QObject):
    networkStatusChanged = pyqtSignal(bool, unicode)
    
    def __init__(self, runLocal, lasagneServer, lasagnePort, ovenLogin, delegate):
        super(LasagneClient, self).__init__()
        self.mutex = loggingMutex("Lasagne Client", qMutex=True)
        self.serverPort = lasagnePort
        self.runLocal = runLocal
        if not self.runLocal:
            self.tunnelHandler = SSHTunnelHandler(lasagneServer, ovenLogin)
        self.initialize(connect=False)
        if not self.runLocal:
            # open tunnel for MySQL
            self.tunnelHandler.openSSHTunnel(3306, 3306)
        self.delegate = delegate
    
    def initialize(self, connect=True):
        with self.mutex:
            self.nameserver = None
            self.server = None
            self.lsaResult = None
            self.lsi = None
            self.topology = None
            self.classifiers = None
            if not self.runLocal:
                self.tunnelHandler.closeAllTunnels()
        
        if connect:
            self.checkConnection()
    
    def close(self):
        if not self.runLocal:
            self.tunnelHandler.close()
            
    def getLSAResult(self):
        with self.mutex:
            if self.lsaResult == None:
                log_info("Loading proxy for LSA result...")
                if self.nameserver == None:
                    self._initializeServer()
                 
                self.server.loadLSAResult()
                try:
                    uri = self.nameserver.lookup(LasagneServer.LSA_RESULT_ID)
                except Pyro4.errors.NamingError:
                    log_error("Could not load LSA result proxy from server.")
                    return None
                uri = self._openSSHTunnel(uri)
                self.lsaResult = Pyro4.Proxy(uri)
                self.lsaResult._pyroBind()
            return self.lsaResult
        
    def getLSI(self):
        with self.mutex:
            if self.lsi == None:
                log_info("Loading proxy for LSI...")
                if self.nameserver == None:
                    self._initializeServer()
                 
                self.server.loadLSI()
                try:
                    uri = self.nameserver.lookup(LasagneServer.LSI_ID)
                except Pyro4.errors.NamingError:
                    log_error("Could not load LSI proxy from server.")
                    return None
                uri = self._openSSHTunnel(uri)
                self.lsi = Pyro4.Proxy(uri)
                self.lsi._pyroBind()
            return self.lsi
    
    def getTopology(self):
        with self.mutex:
            if self.topology == None:
                log_info("Loading proxy for Topology...")
                if self.nameserver == None:
                    self._initializeServer()
                 
                uri = self.nameserver.lookup(LasagneServer.TOPOLOGY_ID)
                uri = self._openSSHTunnel(uri)
                self.topology = Pyro4.Proxy(uri)
                self.topology._pyroBind()
            return self.topology
          
    def getBrains(self):
        with self.mutex:
            if self.classifiers == None:
                log_info("Loading proxy for neural network...")
                if self.nameserver == None:
                    self._initializeServer()
                 
                self.classifiers = []
                try:
                    for i in itertools.count():
                        uri = self.nameserver.lookup(LasagneServer.BRAINS_ID_TEMPLATE % i)
                        uri = self._openSSHTunnel(uri)
                        newBrain = Pyro4.Proxy(uri)
                        newBrain._pyroBind()
                        self.classifiers.append(newBrain)
                except:
                    pass
            return self.classifiers
          
    def __enter__(self):
        return self
    
    def __exit__(self, *_args):
        self.close()
        
    def _initializeServer(self):
        # not called from outside, no lock
        log_debug("Connecting to server...")
        if not self.runLocal:
            pyroPort = self.tunnelHandler.openSSHTunnel(self.serverPort)
        else:
            pyroPort = self.serverPort
        self.nameserver = Pyro4.locateNS(host="127.0.0.1", port=pyroPort)
        log_debug("Creating server object...")
        uri = self.nameserver.lookup(LasagneServer.SERVER_ID)
        uri = self._openSSHTunnel(uri)
        self.server = Pyro4.Proxy(uri)
        self.server._pyroBind()
        
    def _openSSHTunnel(self, uri):
        # not called from outside, no lock
        if not self.runLocal:
            uri.port = self.tunnelHandler.openSSHTunnel(uri.port)
        return uri
    
    @pyqtSlot()
    def checkConnection(self):
        result = None
        with self.mutex:
            if self.server == None:
                try:
                    self._initializeServer()
                except NamingError:
                    log_exception()
                    if self.nameserver == None:
                        result = (False, u"Name Server not reachable")
                    else:
                        result = (False, u"Server Object not found")
            
            if result == None:
                if self.server != None:
                    result = (True, u"")
                else:
                    result = (False, u"Unknown error")
        
        if result != None:
            self.networkStatusChanged.emit(result[0], result[1])
    
    @pyqtSlot()
    def reconnect(self):
        # initialize is locked, no lock here
        self.initialize()
    