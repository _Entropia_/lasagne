from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject
from lasagne import log_exception, convert_string, log_error, log_debug,\
    BASE_CATEGORIES
import contextlib
import shlex
import Pyro4
from lasagne.utils.delegate_cache import DelegateCache

class NetworkHandler(QObject, DelegateCache):
    queryResultsReceived = pyqtSignal(list, list) # list of classifiers, list of rows
    tableOutputReceived = pyqtSignal(list, list, list) #rows, rowIDs, colIDs
    statusChanged = pyqtSignal(unicode)
    queryFinished = pyqtSignal(bool) #true: successful
    choosePage = pyqtSignal(list, int) # list of (page id, page namespace), action ID
    performActionOnID = pyqtSignal(int, int, int) # (page id, page namespace, action id)
    
    def __init__(self, client):
        super(NetworkHandler, self).__init__()
        self.client = client
        self.delegate = client.delegate
        self._lsaResult = None
        self._lsi = None
        self._topology = None
        self._cnx = None
        self._classifiers = None
        
    def _handleException(self, e, desc):
        log_exception(desc)
        self.statusChanged.emit(unicode(e))
        self.queryFinished.emit(False)
        
    def _handleError(self, e):
        log_error(e)
        self.statusChanged.emit(e)
        
    def close(self):
        if self._cnx != None:
            self._cnx.close()
            self._cnx = None
        
    @property
    def cnx(self):
        try:
            if self._cnx == None:
                self._cnx = self.dbHandler.newConnection()
            return self._cnx
        except:
            log_exception("Exception in property 'cnx'")
        
    @property
    def topology(self):
        try:
            if self._topology == None:
                clientTopology = self.client.getTopology()
                if clientTopology != None:
                    self._topology = Pyro4.Proxy(clientTopology._pyroUri)
            return self._topology
        except:
            log_exception("Exception in property 'topology'")
        
    @property
    def lsaResult(self):
        try:
            if self._lsaResult == None:
                clientIndex = self.client.getLSAResult()
                if clientIndex != None:
                    self._lsaResult = Pyro4.Proxy(clientIndex._pyroUri)
            return self._lsaResult
        except:
            log_exception("Exception in property 'lsaResult'")
            
    @property
    def lsi(self):
        try:
            if self._lsi == None:
                clientIndex = self.client.getLSI()
                if clientIndex != None:
                    self._lsi = Pyro4.Proxy(clientIndex._pyroUri)
            return self._lsi
        except:
            log_exception("Exception in property 'lsi'")
        
    @property
    def classifiers(self):
        try:
            if self._classifiers == None:
                clientBrains = self.client.getBrains()
                if clientBrains != None:
                    self._classifiers = [Pyro4.Proxy(clientBrain._pyroUri) for clientBrain in clientBrains]
            return self._classifiers
        except:
            log_exception("Exception in property 'brain'")

    @pyqtSlot(unicode)
    def fullTextSearch(self, query):
        try:
            if self.lsi == None:
                self._handleError("LSA Index is None.")
                self.queryFinished.emit(False)
                return
            
            query = convert_string(query)
            result = self.lsi.search(query, details=True)
            
            self.queryResultsReceived.emit(self.topology.getClassifiers(), result)
            self.queryFinished.emit(True)
        except Exception as e:
            self._handleException(e, "Error during full text search")
        
    @pyqtSlot(unicode)
    def getPages(self, query):
        queryList = self._splitTitleQuery(query)
        if not queryList:
            self.queryFinished.emit(False)
            return
        
        try:
            if self.topology == None:
                self._handleError("Topology is None.")
                self.queryFinished.emit(False)
                return
                
            result = self.topology.getPageDetails(queryList)
            self.queryResultsReceived.emit(self.topology.getClassifiers(), result)
            self.queryFinished.emit(True)
        except Exception as e:
            self._handleException(e, "Error during full text search")
        
    def _splitTitleQuery(self, query):
        queryList = None
        try:
            tmpList = shlex.split(convert_string(query).encode('utf-8'))
            parsedList = []
            for queryTerm in tmpList:
                try:
                    intVal = int(queryTerm)
                    parsedList.append(intVal)
                except:
                    parsedList.append(queryTerm)
            queryList = parsedList
        except:
            log_exception()
        if not queryList:
            self._handleError("Query is empty.")
        return queryList
            
    @pyqtSlot(unicode)
    def getEntrycats(self, query):
        queryList = self._splitTitleQuery(query)
        if not queryList:
            self.queryFinished.emit(False)
            return
        
        try:
            if self.topology == None:
                self._handleError("Topology is None.")
                self.queryFinished.emit(False)
                return
                
            log_debug("Query entry categories for", queryList)
            result, colIDs = self.topology.getEntrycats(queryList, pageIDs = True)
            self.tableOutputReceived.emit(result, [], colIDs)
            self.queryFinished.emit(True)
        except Exception as e:
            self._handleException(e, "Error during full text search")
    
    @pyqtSlot(unicode)
    def getDistances(self, query):
        queryList = self._splitTitleQuery(query)
        if not queryList:
            self.queryFinished.emit(False)
            return
        
        try:
            if self.topology == None:
                self._handleError("Topology is None.")
                self.queryFinished.emit(False)
                return
                
            log_debug("Query distances for", queryList)
            
            header = ["Page Title", "Type"]
            header.extend(BASE_CATEGORIES)
            rows = [header]
            
            results = self.topology.getAllDistances(queryList)
            rowIDs = [aTup[0] for aTup in results]
            colIDs = [None, None]
            colIDs.extend(self.baseCategories)
            
            for _pID, pNs, pTitle, distances in results:
                row = [pTitle, "article" if pNs == 0 else "category" if pNs == 14 else "<unknown>"]
                row.extend(distances)
                rows.append(row)
            
            self.tableOutputReceived.emit(rows, rowIDs, colIDs)
            self.queryFinished.emit(True)
        except Exception as e:
            self._handleException(e, "Error during full text search")
                
    @pyqtSlot(unicode, int, int)
    def preparePageID(self, query, namespaceSelection, actionID):
        try:
            query = convert_string(query)
            if self.topology == None:
                self._handleError("Topology is None.")
                self.queryFinished.emit(False)
                return
            
            try:
                # check if this is a page ID
                intVal = int(query)
                query = intVal
            except:
                pass
                
            log_debug("Preparing category paths for", query)
            candidates = self.topology.getCandidates(query, namespaceSelection)
            
            if len(candidates) == 0:
                self.statusChanged.emit("No pages found.")
                self.queryFinished.emit(False)
            elif len(candidates) == 1:
                self.performActionOnID.emit(candidates[0][0], candidates[0][1], actionID)
            else:
                # ask user
                self.statusChanged.emit("Multiple candidates found")
                with contextlib.closing(self.dbHandler.newConnection()) as cnx:
                    with contextlib.closing(cnx.cursor(prepared=True)) as cursor:
                        candidates = [(self.dbHandler.getPageName(pID, cursor), pID, pNs) for pID, pNs in candidates]
                        self.choosePage.emit(candidates, actionID)
                # not finished yet
                return
            
        except Exception as e:
            self._handleException(e, "Error during category path preparation")
            
    @pyqtSlot(int, int)
    def getCategoryPaths(self, pageID, pageNamespace):
        try:
            if self.topology == None:
                self._handleError("Topology is None.")
                self.queryFinished.emit(False)
                return
                
            log_debug("Get category paths for page ID %d (%d)" % (pageID, pageNamespace))
            result, colIDs = self.topology.getCategoryPaths(pageID, pageNamespace, pageIDs = True)
            self.tableOutputReceived.emit(result, [], colIDs)
            self.queryFinished.emit(True)
        except Exception as e:
            self._handleException(e, "Error during category path computation")

    @pyqtSlot(int)
    def getSimilarArticles(self, articleID):
        try:
            if self.lsi == None:
                self._handleError("LSA Index is None.")
                self.queryFinished.emit(False)
                return
                    
            result = self.lsi.searchSimilarArticles(articleID, details=True)
            
            self.queryResultsReceived.emit(self.topology.getClassifiers(), result)
            self.queryFinished.emit(True)
        except Exception as e:
            self._handleException(e, "Error during full text search")
    
    @pyqtSlot(unicode)
    def classifyPlainText(self, query):
        try:
            if self.classifiers == None:
                self._handleError("I don't own a brain.")
                self.queryFinished.emit(False)
                return
            
            if self.lsaResult == None:
                self._handleError("LSA Index is None.")
                self.queryFinished.emit(False)
                return

            queryVec = self.lsaResult.generateQueryVector(query, pythonList=True)
            categorizations = [classifier.classify(queryVec) for classifier in self.classifiers]
            
            rows = []
            rows.append(["Brain", "Categorization"])
            for i, categorization in enumerate(categorizations):
                categories = ', '.join(BASE_CATEGORIES[j] for j in range(len(BASE_CATEGORIES)) if categorization[j] == 1)
                rows.append(["Brain %d" % i, categories])
            
            self.tableOutputReceived.emit(rows, [], [])
            self.queryFinished.emit(True)
        except Exception as e:
            self._handleException(e, "Error during full text search")
    