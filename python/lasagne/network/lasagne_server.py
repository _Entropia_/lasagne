import Pyro4
from lasagne import log_info, log_error, log_debug
from lasagne.search.lsi import LSI
from lasagne.file import default_paths
from lasagne.utils.topology import Topology
from lasagne.search.lsa_result import LSAResult
from lasagne.utils.logging_mutex import loggingMutex
from lasagne.classify.brain import Brain
from lasagne.exe.run_svm import SVMClassifier

class LasagneServer(object):
    SERVER_ID = "lasagne.server"
    LSI_ID = "lasagne.lsi"
    LSA_RESULT_ID = "lasagne.lsaResult"
    TOPOLOGY_ID = "lasagne.topology"
    BRAINS_ID_TEMPLATE = "lasagne.brain%d"
    
    def __init__(self,
                 host,
                 port,
                 delegate,
                 lsaVectorsFile=default_paths.LSA_VECTORS_FILE,
                 termDictFile=default_paths.TERM_DICTIONARY_FILE,
                 lsaCorpusFile=default_paths.LSA_CORPUS_FILE,
                 brainFiles=default_paths.BRAIN_FILES,
                 svmFiles=default_paths.SVM_FILES):
        self.host = host
        self.port = port
        self.lsaVectorsFile = lsaVectorsFile
        self.termDictFile = termDictFile
        self.lsaCorpusFile = lsaCorpusFile

        log_debug("Loading neural network...")
        
        self.classifiers = [Brain(brainFile) for brainFile in brainFiles]
        self.classifiers.extend(SVMClassifier(svmFile) for svmFile in svmFiles)
        
        self.topology = Topology(delegate, self.classifiers)
        
        self.lsaResult = None
        self.lsi = None
        
        self.mutex = loggingMutex("Lasagne Server")
        
    def _doLoadLSAResult(self):
        if self.lsaResult != None:
            # don't load twice
            log_info("LSA result already loaded.")
            return
        if self.termDictFile and self.lsaCorpusFile:
            log_info("Loading LSA results...")
            self.lsaResult = LSAResult(self.termDictFile, self.lsaCorpusFile)
            uri = self.daemon.register(self.lsaResult)
            self.ns.register(self.LSA_RESULT_ID, uri)
            log_info("LSA result ready.")
            return True
        else:
            log_error("Cannot load LSA result, input file is missing:", "LSA corpus" if not self.lsaCorpusFile else "Term dictionary")
        return False
        
    def loadLSAResult(self):
        with self.mutex:
            return self._doLoadLSAResult()
    
    def loadLSI(self):
        with self.mutex:
            if self.lsi != None:
                # don't load twice
                log_info("LSI already loaded.")
                return
            if self.lsaResult == None:
                if not self._doLoadLSAResult():
                    log_error("Could not load LSI.")
                    return
            
            if self.lsaVectorsFile:
                log_info("Loading LSI...")
                self.lsi = LSI(self.lsaVectorsFile, self.lsaResult, self.topology)
                uri = self.daemon.register(self.lsi)
                self.ns.register(self.LSI_ID, uri)
                self.topology.lsi = self.lsi
                log_info("LSI ready.")
            else:
                log_error("Cannot load LSI, input file is missing: LSA vectors")
    
    def startServer(self, preload=False):
        self.daemon = Pyro4.Daemon()
        self.ns = Pyro4.locateNS(self.host, self.port)
        
        # register server
        uri = self.daemon.register(self)
        self.ns.register(self.SERVER_ID, uri)
        
        # register topology
        uri = self.daemon.register(self.topology)
        self.ns.register(self.TOPOLOGY_ID, uri)
        
        # register brain
        for i, brain in enumerate(self.classifiers):
            uri = self.daemon.register(brain)
            self.ns.register(self.BRAINS_ID_TEMPLATE % i, uri)
        
        if preload:
            log_info("Preloading data...")
            self.topology.delegate.categoryDists
            self.topology.delegate.entryCats
            self.loadLSI()
            self.topology.delegate.baseCategories
            self.topology.delegate.dbHandler
            
        log_info("Lasagne server ready.")
            
        self.daemon.requestLoop()
