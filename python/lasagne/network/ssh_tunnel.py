from lasagne import log_info, log_exception, log_debug
import time
import subprocess
import shlex

class SSHTunnelHandler(object):
    def __init__(self, serverHostname, serverLogin):
        self.serverHostname = serverHostname
        self.serverLogin = serverLogin
        
        self.openTunnels = []
        self.portMapping = {}  # mapping from remote port to local port
    
    def closeAllTunnels(self):
        log_debug("Closing all SSH tunnels")
        for process in self.openTunnels:
            process.kill()
        self.openTunnels = []
    
    def close(self):
        self.closeAllTunnels()
    
    def _getOpenPort(self):
        import socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(("", 0))
        s.listen(1)
        port = s.getsockname()[1]
        s.close()
        return port
    
    def openSSHTunnel(self, remotePort, localPort = None):
        if remotePort in self.portMapping:
            return self.portMapping[remotePort]
        
        if localPort == None:
            localPort = self._getOpenPort()
        self.portMapping[remotePort] = localPort
        
        log_info("Opening ssh tunnel from local port", localPort, "to remote port", remotePort)
        cmd = 'ssh -C -N %(login)s@%(host)s -L %(localPort)d:127.0.0.1:%(remotePort)d' % {'host':self.serverHostname,
                                                                                          'login':self.serverLogin,
                                                                                          'localPort':localPort,
                                                                                          'remotePort':remotePort}
        try:
            log_debug(cmd)
            self.openTunnels.append(subprocess.Popen(shlex.split(cmd)))
            # Hack - wait until port is probably open
            time.sleep(1)
        except:
            log_exception()
        return localPort
