BASE_CATEGORIES = sorted(["Electronics", "Health", "Culture", "Geography", "History", "Mathematics", "Biology", "Physics", "Chemistry", "Biography", "Philosophy", "Religion", "Politics", "Military", "Communication", "Transport"])

import logging

class _lasagne_logger:
    lasagne_logger = None
    streamHandler = None
    logfileHandler = None
     
    @classmethod
    def get_singleton_logger(cls):
        if cls.lasagne_logger == None:
            streamHandler = logging.StreamHandler()
            streamHandler.setFormatter(logging.Formatter("[%(levelname)7s] %(message)s"))
            
            cls.lasagne_logger = logging.getLogger()
            cls.lasagne_logger.setLevel(logging.DEBUG)
            cls.lasagne_logger.addHandler(streamHandler)
            
        return cls.lasagne_logger
    
    @classmethod
    def setLogFile(cls, path):
        if cls.logfileHandler != None:
            # close old handler
            cls.lasagne_logger.removeHandler(cls.logfileHandler)
            cls.logfileHandler.close()
        
        cls.logfileHandler = logging.FileHandler(path)
        cls.logfileHandler.setFormatter(logging.Formatter('%(asctime)s : %(levelname)7s : %(message)s'))
        cls.logfileHandler.setLevel(logging.DEBUG)
        cls.lasagne_logger.addHandler(cls.logfileHandler)

#initialize logger
_lasagne_logger.get_singleton_logger()

def convert_string(string):
    if type(string) == unicode:
        return string
    elif type(string) == str:
        return string.decode('utf-8')
    return unicode(string)

def _get_logger():
    return _lasagne_logger.get_singleton_logger()

def setLoggingLevel(newLevel):
    # ensure logger is initialized
    _get_logger()
    _lasagne_logger.lasagne_logger.setLevel(newLevel)
    
def setLogFile(path):
    _lasagne_logger.setLogFile(path)

def _generate_string(*s):
    return u" ".join(x if type(x) in (str, unicode) else str(x) for x in s)

def log_exception(*s):
    _get_logger().exception(_generate_string(*s))
    
def log_critical(*s):
    _get_logger().critical(_generate_string(*s))
    
def log_error(*s):
    _get_logger().error(_generate_string(*s))
    
def log_warning(*s):
    _get_logger().warn(_generate_string(*s))
    
def log_info(*s):
    _get_logger().info(_generate_string(*s))
    
def log_debug(*s):
    _get_logger().debug(_generate_string(*s))

