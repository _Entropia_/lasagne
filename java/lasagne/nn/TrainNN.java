package lasagne.nn;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Iterator;

import lasagne.data.ArticleInfo;
import lasagne.data.ArticleIterable;
import lasagne.data.CSVArticleIterable;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.util.TransferFunctionType;

import corny.args.Arguments;
import corny.args.CommandLineOption;

public class TrainNN {

	static CommandLineOption<String> inFile = Arguments.addStringOption('i',
			"in", "Read NN from File", "Path to NN file", "");
	static CommandLineOption<String> outFile = Arguments.addStringOption('o',
			"out", "Write trained NN to file", "Path to NN file", "out.nnet");

	static CommandLineOption<Double> learningRate = Arguments.addDoubleOption(
			'l', "learning-rate", "Set learning rate", "Learning rate", 0.2);
	static CommandLineOption<Double> momentum = Arguments.addDoubleOption('m',
			"momentum", "Set momentum for learning", "Momentum", 0.7);
	static CommandLineOption<Double> maxError = Arguments
			.addDoubleOption('e', "max-error",
					"Maximum error at which learning stops", "Error", 0.01);
	static CommandLineOption<Integer> maxIt = Arguments
			.addIntegerOption(
					't',
					"max-iterations",
					"Set the number of iterations at which learning will stop regardless of error",
					"Number of iterations", 0);

	static CommandLineOption<Integer> hiddenNeurons = Arguments
			.addIntegerOption('h', "hidden-neurons",
					"Set the number of neurons in the hidden layer",
					"Number of neurons in the hidden layer", 400);

	static CommandLineOption<TransferFunctionType> tft = Arguments
			.addChoiceOption('f', "transfer-function-type",
					"Transfer Function Type", "type",
					TransferFunctionType.values(), TransferFunctionType.SIGMOID);

	public static void main(String[] args) throws FileNotFoundException {
		if (!Arguments.parse(args))
			return;

		ArticleIterable ai = new CSVArticleIterable(new File("test.tsv"), '\t');
		int vl = ai.getVectorLength(); // should be 400

		NeuralNetwork mlp;
		// if input file is given, create NN from file
		if (inFile.getValue() != "") {
			System.out.print("Loading Neural Network from " + inFile.getValue()
					+ " ... ");
			mlp = NeuralNetwork.createFromFile(inFile.getValue());
			System.out.print("Done!\n");
		} else {
			System.out.print("Generating Neural Network ... ");
			// create multi layer perceptron from scratch
			mlp = new MultiLayerPerceptron(tft.getValue(), vl,
					hiddenNeurons.getValue(), 16);
			MomentumBackpropagation learningRule = (MomentumBackpropagation) mlp
					.getLearningRule();
			learningRule.setLearningRate(learningRate.getValue());
			learningRule.setMomentum(momentum.getValue());
			learningRule.setMaxError(maxError.getValue());
			learningRule.setMaxIterations(maxIt.getValue());
			// mlp.setLearningRule(learningRule); // probably unnecessary
			System.out.print("Done!\n");
		}

		System.out.print("Generating Training Set ... ");
		// @TODO: build training sets for re-use and consistency
		// create training set (logical XOR function)
		DataSet trainingSet = new DataSet(vl, 16);
		Iterator<ArticleInfo> it = ai.iterator();
		while (it.hasNext()) {
			ArticleInfo inf = it.next();
			// construct expected output vector
			double[] out = new double[16];
			for (short i = 0; i < 15; ++i) {
				out[i] = (inf.hasCategory(i) ? 1.0 : 0.0);
			}
			trainingSet.addRow(new DataSetRow(inf.getVector(), out));
		}
		System.out.print("Done!\n");

		System.out.print("Learning ... ");
		// learn the training set
		mlp.learn(trainingSet);
		System.out.print("Done!\n");

		// @TODO: make pretty output (error, iterations, etc.)

		System.out.print("Storing Neural Network to " + outFile.getValue()
				+ " ... ");
		// save trained neural network
		mlp.save(outFile.getValue());
		System.out.print("Done!\n");
	}
}