package lasagne.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Iterable which iterates over CSV-based files. Refer to the documentation of
 * {@link CSVIterator} for more details.
 * 
 * @author Corny
 * 
 */
public class CSVIterable implements Iterable<String[]> {
	private final File file;
	private final int[] columns;
	private final char delimiter, quoteChar;
	private final boolean ignoreFirstLine;

	/**
	 * Creates new CSV iterable. Refer to the documentation of
	 * {@link CSVIterator} for more details.
	 * 
	 * @param reader
	 *            Input Reader
	 * @param delimiter
	 *            Column delimiter.
	 * @param quoteChar
	 *            Character to quote strings in a column. Usually '"'.
	 * @param ignoreFirstLine
	 *            true to omit the first line of the input reader.
	 * @param columns
	 *            The columns to extract, as a list of integers. 0 corresponds
	 *            to the first column.
	 */
	public CSVIterable(File file, char delimiter, char quoteChar,
			boolean ignoreFirstLine, int... columns)
			throws FileNotFoundException {
		this.file = file;
		this.columns = columns;
		this.delimiter = delimiter;
		this.quoteChar = quoteChar;
		this.ignoreFirstLine = ignoreFirstLine;
	}

	public CSVIterator iterator() {
		try {
			return new CSVIterator(new FileReader(file), delimiter, quoteChar,
					ignoreFirstLine, columns);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

}
