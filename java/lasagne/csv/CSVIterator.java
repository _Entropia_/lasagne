package lasagne.csv;

import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Iterator which iterates over a CSV based input stream line by line and
 * returns values from user defined columns.
 * <p>
 * You may specify which columns to extract. The order of the extracted columns
 * always equals the order of the columns in the file.
 * <p>
 * Additional spaces FOLLOWING the delimiting character are skipped. Repeating
 * delimiting characters result in empty columns, except if you specify the
 * space character (' ') as the delimiting character. If you specify a space as
 * the delimiting char, multiple spaces are considered a SINGLE delimiting
 * sequence.
 * <p>
 * Quoting the contents of a column is not necessary. If the content is quoted,
 * ALL characters between two quote characters (usually '"') build the content
 * of the column, including line breaks.
 * <p>
 * If you want to insert the quoting character itself into the column content,
 * the content needs to be quoted. Then, inside a quoted column, use a sequence
 * of two quoting characters to insert the quote character in the column
 * content. For example, """Test""" results in "Test", while ""Test"" results in
 * an empty string.
 * <p>
 * The input reader will be closed as soon as the last line is read. You can use
 * {@link #close()} to close the input reader manually.
 * 
 * @author Corny
 */
public class CSVIterator implements Iterator<String[]>, Closeable {
	enum NextState {
		TRUE, FALSE, UNTESTED;
	}

	private final Set<Integer> columns;
	private final char delimiter, quoteChar;
	// private Scanner fileScanner;
	private final Reader fileReader;
	private String[] nextLine;
	private int numColumns = -1;
	private NextState state = NextState.UNTESTED;

	/**
	 * Create a new CSV Iterator.
	 * 
	 * @param reader
	 *            Input Reader
	 * @param delimiter
	 *            Column delimiter.
	 * @param quoteChar
	 *            Character to quote strings in a column. Usually '"'.
	 * @param ignoreFirstLine
	 *            true to omit the first line of the input reader.
	 * @param columns
	 *            The columns to extract, as a list of integers. 0 corresponds
	 *            to the first column.
	 */
	public CSVIterator(Reader reader, char delimiter, char quoteChar,
			boolean ignoreFirstLine, int... columns) {
		if (columns != null && columns.length > 0) {
			this.columns = new HashSet<Integer>(columns.length);
			for (int col : columns) {
				this.columns.add(col);
			}
			numColumns = columns.length;
		} else {
			this.columns = null;
		}
		this.delimiter = delimiter;
		this.quoteChar = quoteChar;
		// fileScanner = new Scanner(inputStream);
		this.fileReader = reader;
		if (ignoreFirstLine) {
			try {
				nextLine(true);
			} catch (IOException e) {
			}
		}
	}

	public boolean hasNext() {
		switch (state) {
		case TRUE:
			return true;
		case FALSE:
			return false;
		default:
			boolean hasNext;
			try {
				if (fileReader.ready()) {
					nextLine(false);
					hasNext = true;
				} else {
					hasNext = false;
				}
			} catch (IOException e) {
				hasNext = false;
			}
			state = hasNext ? NextState.TRUE : NextState.FALSE;
			return hasNext;
		}
	}

	public String[] next() {
		if (nextLine == null) {
			try {
				nextLine(false);
			} catch (IOException e) {
				return null;
			}
		}
		String[] ret = nextLine;
		nextLine = null;
		state = NextState.UNTESTED;
		return ret;
	}

	private int columnFinished(List<String> line, StringBuffer currentToken,
			int columnIndex, boolean skip) {
		if (skip) {
			return 0;
		}
		if (columns == null || columns.contains(columnIndex)) {
			line.add(currentToken.toString());
		}
		++columnIndex;
		return columnIndex;
	}

	private void nextLine(boolean skip) throws IOException {
		List<String> line = null;
		StringBuffer currentToken = null;
		if (!skip) {
			line = new ArrayList<String>(numColumns != -1 ? numColumns : 5);
			currentToken = new StringBuffer();
		}

		boolean insideQuote = false, insideToken = false;
		int columnIndex = 0;
		int currentChar;
		while (true) {
			currentChar = fileReader.read();
			if (currentChar == -1) {
				if (insideToken) {
					columnIndex = columnFinished(line, currentToken,
							columnIndex, skip);
					insideToken = false;
				}
				break;
			} else if (currentChar == quoteChar) {
				if (insideQuote) {
					// is termination or escaped quote
					int nextChar = fileReader.read();
					if (nextChar != quoteChar) {
						// was termination
						insideQuote = false;
						columnIndex = columnFinished(line, currentToken,
								columnIndex, skip);
						insideToken = false;

						// check if next token is delimiter or new line
						if (nextChar == -1 || nextChar == '\n') {
							currentChar = nextChar;
							break;
						}
						if (!skip) {
							currentToken = new StringBuffer();
						}
						if (nextChar != delimiter) {
							// invalid csv format, missing delimiter
							while (nextChar != delimiter) {
								nextChar = fileReader.read();
								if (nextChar == -1 || nextChar == '\n') {
									currentChar = nextChar;
									break;
								}
							}
						}
					} else if (!skip) {
						// is escaped quote
						currentToken.append(quoteChar);
					}
				} else {
					// should be beginning of quoted token
					if (insideToken) {
						// invalid formatted csv, quote inside non-quoted
						// token
						if (!skip) {
							currentToken.append(quoteChar);
						}
					} else {
						insideQuote = true;
						insideToken = true;
					}
				}
			} else if (isWhitespace(currentChar) && !insideToken) {
				// skip whitespace following a delimiting character
				continue;
			} else if (insideQuote
					|| (currentChar != delimiter && currentChar != '\n')) {
				insideToken = true;
				if (!skip) {
					currentToken.append((char) currentChar);
				}
			} else {
				// is delimiter or new line, token finished
				columnIndex = columnFinished(line, currentToken, columnIndex,
						skip);
				insideToken = false;
				if (currentChar == '\n') {
					break;
				}
				if (!skip) {
					currentToken = new StringBuffer();
				}
			}
		}
		if (!skip && line.size() > 0) {
			nextLine = line.toArray(new String[0]);
			if (numColumns == -1) {
				numColumns = nextLine.length;
			}
		}
		if (currentChar == -1) {
			fileReader.close();
		}
	}

	private boolean isWhitespace(int currentChar) {
		return currentChar == ' ' || (currentChar == '\t' && delimiter != '\t');
	}

	public void remove() {
		throw new UnsupportedOperationException("Source is read-only.");
	}

	/**
	 * Skips a number of lines. It is safe to skip more lines than the input
	 * reader contains.
	 * 
	 * @param lines
	 *            Number of lines to skip
	 */
	public void skip(int lines) {
		if (lines <= 0) {
			return;
		}
		try {
			nextLine = null;
			for (int i = 0; i < lines; ++i) {
				nextLine(true);
			}
		} catch (IOException e) {
			// Probably not enough lines to skip
		}
	}

	@Override
	public void close() {
		try {
			fileReader.close();
		} catch (IOException e) {
			// Probably already closed
		}
	}

	public static void main(String[] args) throws IOException {
		FileReader fileReader2 = new FileReader(new File(
				"/Users/Corny/Desktop/test.tsv"));
		CSVIterator it = new CSVIterator(fileReader2, '\t', '"', false);

		for (String s : it.next()) {
			System.out.print("\"" + s + "\",");
		}
		System.out.println();

		it.skip(2);

		while (it.hasNext()) {
			for (String s : it.next()) {
				System.out.print("\"" + s + "\",");
			}
			System.out.println();
		}
		it.close();
		fileReader2.close();
	}
}