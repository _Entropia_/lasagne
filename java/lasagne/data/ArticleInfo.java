package lasagne.data;

/**
 * Represents a Wikipedia article
 * 
 * @author Corny
 */
public interface ArticleInfo {
	/**
	 * 
	 * @return get the Article ID
	 */
	int getID();

	/**
	 * 
	 * @return get the LSI representation vector
	 */
	double[] getVector();

	/**
	 * 
	 * @return get the Category Bit Vector
	 */
	short getCategories();

	/**
	 * Check if Article has given category.
	 * 
	 * @param c
	 *            category ID. 0<=c<=15
	 * @return true if corresponding bit in {@link #getCategories()} is 1.
	 */
	boolean hasCategory(short c);
}
