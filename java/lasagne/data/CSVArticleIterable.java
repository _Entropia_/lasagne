package lasagne.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidParameterException;
import java.util.Iterator;

import lasagne.csv.CSVIterable;
import lasagne.csv.CSVIterator;

/**
 * A CSV-based {@link ArticleIterable}
 * 
 * @author Corny
 */
public class CSVArticleIterable implements ArticleIterable {
	private final CSVIterable csvIter;
	private int vectorLength = -1;

	/**
	 * Creates new instance.
	 * 
	 * @param file
	 *            Data file
	 * @param delimiter
	 *            Data file separator (must not be '|')
	 * @throws FileNotFoundException
	 *             If the file does not exist
	 */
	public CSVArticleIterable(File file, char delimiter)
			throws FileNotFoundException {
		if (delimiter == '|') {
			throw new InvalidParameterException(
					"Cannot use '|' as the delimiter;"
							+ " it is used for separating the categories.");
		}
		csvIter = new CSVIterable(file, delimiter, '"', false, null);
	}

	@Override
	public Iterator<ArticleInfo> iterator() {
		return new CSVArticleIterator(0);
	}

	@Override
	public Iterator<ArticleInfo> iteratorAt(int index) {
		return new CSVArticleIterator(index);
	}

	@Override
	public int getVectorLength() {
		if (vectorLength == -1) {
			vectorLength = iterator().next().getVector().length;
		}
		return vectorLength;
	}

	class CSVArticleIterator implements Iterator<ArticleInfo> {
		private CSVIterator iter;
		private final int startIndex;
		private int nextIndex;
		private boolean wrapped = false;

		public CSVArticleIterator(int startIndex) {
			this.startIndex = startIndex;
			this.nextIndex = startIndex;
			iter = csvIter.iterator();
			iter.skip(startIndex);
		}

		@Override
		public boolean hasNext() {
			if (startIndex == 0) {
				return iter.hasNext();
			} else {
				return !wrapped || startIndex != nextIndex;
			}
		}

		@Override
		public ArticleInfo next() {
			if (!iter.hasNext()) {
				iter = csvIter.iterator();
				nextIndex = 0;
				wrapped = true;
			}

			String[] cols = iter.next();
			int id = Integer.valueOf(cols[0]);
			short categories = 0;
			String[] cats = cols[1].split("\\|");
			for (String aCat : cats) {
				categories |= (1 << Short.valueOf(aCat));
			}
			double[] vector = new double[cols.length - 2];
			for (int i = 2; i < cols.length; ++i) {
				vector[i - 2] = Double.valueOf(cols[i]);
			}

			if (iter.hasNext()) {
				++nextIndex;
			} else {
				nextIndex = 0;
			}
			return new ArticleInfoImpl(id, vector, categories);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}

	public static void main(String[] args) throws FileNotFoundException {
		Iterator<ArticleInfo> it = new CSVArticleIterable(new File("test.tsv"),
				'\t').iteratorAt(3);
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}
}
