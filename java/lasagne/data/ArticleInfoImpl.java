package lasagne.data;

/**
 * Default {@link ArticleInfo} implementation.
 * 
 * @author Corny
 */
public class ArticleInfoImpl implements ArticleInfo {
	private final int id;
	private final double[] vector;
	private final short categories;

	/**
	 * Creates a new ArticleInfoImpl instance.
	 * 
	 * @param id
	 *            Article ID
	 * @param vector
	 *            Document vector
	 * @param categories
	 *            Category bit vector
	 */
	public ArticleInfoImpl(int id, double[] vector, short categories) {
		this.id = id;
		this.vector = vector;
		this.categories = categories;
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public double[] getVector() {
		return vector;
	}

	@Override
	public short getCategories() {
		return categories;
	}

	@Override
	public boolean hasCategory(short c) {
		return (categories & (1 << c)) != 0;
	}
	
	@Override
	public String toString() {
		return "Article " + id;
	}
}
