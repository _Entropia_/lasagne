package lasagne.data;

import java.util.Iterator;

/**
 * Represents iterable collection of Articles and their Info.
 * 
 * @author Oliver
 * 
 */
public interface ArticleIterable extends Iterable<ArticleInfo> {
	/**
	 * Start Iterator at specified Article.
	 * 
	 * @param index
	 *            Article Index to start at.
	 * @return Iterator.
	 */
	Iterator<ArticleInfo> iteratorAt(int index);

	/**
	 * Get length of Article Vectors
	 * 
	 * @return number of values in Article Vector
	 */
	int getVectorLength();
}
